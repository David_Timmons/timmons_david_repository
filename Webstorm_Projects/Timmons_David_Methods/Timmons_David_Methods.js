alert("Welcome to Problem 1, Painting A Wall");

var width = prompt("What is the width of the wall? Please enter a number value. ");

while(width === "" || isNaN(width))
{
    width = prompt("You did not enter a valid input. Please enter a number value. ");
}

var height = prompt("What is the height of the wall? Please enter a number value. ");

while(height === "" || isNaN(height))
{
    height = prompt("You did not enter a valid input. Please enter a number value. ");
}

var coats = prompt("How many coats of paint will the wall require? Please enter a number value. ");

while(coats === "" || isNaN(coats))
{
    coats = prompt("You did not enter a valid input. Please enter a number value. ");
}

var cover = prompt("How many square feet will one gallon of paint cover? Please enter a number value. ");

while(cover === "" || isNaN(cover))
{
    cover = prompt("You did not enter a valid input. Please enter a number value. ");
}

var area = calcArea(width, height);
var paintNeeded = calcFeetSquared(area, coats, cover);

alert("The area of the wall is " + area + " and you need " + paintNeeded + " gallons of paint for " + coats + " coats of paint on it. ");

alert("Welcome to problem 2, Stung!");

var weight = prompt("How much do you weigh? Please enter a number value. ");

while(weight === "" || isNaN(weight))
{
    weight = prompt("You did not enter a valid input. Please enter a number value. ");
}

var toKillYouDead = toKillYou(weight);

alert("With a weight of " + weight + " it will take " + toKillYouDead + " bee stings to kill you. " );

alert("Welcome to problem 3, Welcome to problem 3, Reverse It! ");

var myArray = ["Red", "Orange", "Yellow", "Green", "Blue", "Indigo", "Violet"];
var newArray = returnArray(myArray);

for(var i = 0; i < newArray.length; i++)
{
   alert(newArray[i]);
}

alert("Thank You. ");


//PROBLEM 1 FUNCTIONS

function calcArea(width ,height)
{
    var area = width * height;
    return area;
}
function calcFeetSquared(area, coats, cover)
{
    var feetCovered = area * coats / cover;
    return feetCovered
}

//PROBLEM 2 FUNCTIONS

function toKillYou(weight)
{
    var dead = weight * 9;
    return dead;
}

//PROBLEM 3 Functions

function returnArray(myArray)
{
    var sendArray = [];
    var k = sendArray.length - 1;

    for(var i = myArray.length; i >=0; i--)
    {
        sendArray[k] = myArray[i];
        k++;
    }

    return sendArray;
}