//I have learned quite a bit since I originally completed this assignment
//so there will likely be far less lines of code in this than the original

//labeling the section
alert("Welcome to Problem #1, Temperature Converter!");

//prompt the user for the first piece of user input
var temp = prompt("Please enter a temperature in integer form, and press okay. ");

//validate user input
while (temp === "" || isNaN(temp))
{
    alert("You did not enter a valid input. ");
    temp = prompt("Please enter a temperature in integer form, and press okay. ")
}
//prompt the user for the second piece of user input
var choice = prompt("Now please enter either the character F or C for Fahrenheit or Celsius or type Exit to quit. ").toUpperCase();

//validate user input
//create loop with escape condition
while (choice !== "EXIT")
{
    //condition 1
    if (choice === "C")
    {
        var tempC = ((temp - 32) / 9) * 5;
        alert(temp + " converted from Fahrenheit to Celsius is " + Math.round(tempC) + " degrees. ");
        choice = prompt("Please enter F, C, or Exit. ").toUpperCase();
    }
    //condition 2
    else if (choice === "F")
    {
        var tempF = ((temp / 5) * 9) + 32;
        alert(temp + " converted from Celsius to Fahrenheit is " + Math.round(tempF) + " degrees. ");
        choice = prompt("Please enter F, C, or Exit. ").toUpperCase();
    }
    //invalid condition
    else
    {
        choice = prompt("Please enter F, C, or Exit. ").toUpperCase();
    }
}

//labeling the section
alert("Welcome to Problem #2, Last Chance For Gas\r\nWe're going to find out if you have enough gas to go somewhere. ");

//prompt for first user input
var tankSize = prompt("How many gallons of gas can your cars gas tank hold? Enter a number value. ");

//validate user input
while (tankSize === "" || isNaN(tankSize))
{
    alert("You did not enter a valid input. ");
    tankSize = prompt("How many gallons of gas can your cars gas tank hold? Enter a number value. ")
}

//prompt for second user input
var fullness = prompt("What percentage of your gas tank is currently full? Enter an INTEGER value. ");

//validate user input
while (fullness === "" || isNaN(fullness))
{
    alert("You did not enter a valid input. ");
    fullness = prompt("What percentage of your cars gas tank is currently full? Enter an INTEGER value. ")
}

//prompt for third user input
var mPG = prompt("How many miles does your car get to the gallon? Enter a number value. ");

//validate user input
while (mPG === "" || isNaN(mPG))
{
    alert("You did not enter a valid input. ");
    mPG = prompt("How many miles does your car get to the gallon? Enter a number value. ")
}

//prompt for a new user input, not in the original assignment
var distance = prompt("How far do you need to get? Enter a number value. ");

//validate new user input
while(distance === "" || isNaN(distance))
{
    alert("You did not enter a valid input. ");
    distance = prompt("How far do you need to get? Enter a number value. ")
}

//performing arithmetic operations on collected variables, storing in new variables
var gasInTank = tankSize * (fullness / 100);
var availableDistance = mPG * gasInTank;

//final conditional statement using gathered information
if(availableDistance >= distance)
{
    alert("You have enough gas to make it to your destination. ");
}
else
{
    alert("You do not have enough gas, and will surely die of thirst somewhere in the desert. ");
}

//labeling the section
alert("Welcome To Problem 3, Grade Letter Calculator");

//creating an array of possible letter grades
var gradeArray = ["F", "D", "C", "B", "A"];

//prompting for user input
var grade = prompt("What number grade did you receive? ");

//validating user input
while(grade < 0 || grade > 100 || isNaN(grade) || grade === "")
{
    alert("Grade cannot be less than 0 or more than 100, and must be entered in number form. ");
    grade = prompt("What number grade did you receive? ");
}

//providing output based on user input
if(grade < 69)
{
    alert("You received a letter grade of " + gradeArray[0] + " and failed the course. ")
}
else if (grade < 72)
{
    alert("You received a letter grade of " + gradeArray[1] + " and passed the course. ")
}
else if (grade < 79)
{
    alert("You received a letter grade of " + gradeArray[2] + " and passed the course. ")
}
else if (grade < 89)
{
    alert("You received a letter grade of " + gradeArray[3] + " and passed the course. ")
}
else
{
    alert("You received a letter grade of " + gradeArray[4] + " and passed the course. ")
}

//labeling the section
alert("Welcome to problem 4, Discount Double Check. ");

//prompting for user input
var firstItem = prompt("Please enter the cost of your first item. ");

//validating user input
while(firstItem === "" || isNaN(firstItem))
{
    alert("You did not enter a number value. ");
    firstItem = prompt("Please enter the cost of your first item. ");
}

//prompting for user input
var secondItem = prompt("Please enter the cost of your second item. ");

//validating user input
while(secondItem === "" || isNaN(secondItem))
{
    alert("You did not enter a number value. ");
    secondItem = prompt("Please enter the cost of your second item. ");
}

//declaring variables to perform arithmetic operations on from user input
var total;
total = parseInt(firstItem) + parseInt(secondItem);
var discountArray = [.05, .1 ];
var largeDiscountTotal = total - (total * discountArray[1]);
var smallDiscountTotal = total - (total * discountArray[0]);

if (total >= 100)
{
    alert("Your total with these two items is $" + total + " with a discount of %" + discountArray[1] + "is $" + largeDiscountTotal + ".");
}
else if(total >= 50)
{
    alert("Your total with these two items is $" + total + " with a discount of %" + discountArray[0] + "is $" + smallDiscountTotal + ".");
}
else
    {
        alert("Your total is $" + total + ".");
    }





















