//labeling the section
alert("Welcome to Problem 1, Tire Pressure!");

//gathering input
var frontLeft = prompt("What is the pressure of the front left tire? ");

//validating user input
while (frontLeft === "" || isNaN(frontLeft))
{
    frontLeft = prompt("You did not enter a valid input. What is the pressure of the front left tire?");
}
//gathering input
var frontRight = prompt("What is the pressure of the front right tire? ");

//validating user input
while (frontRight === "" || isNaN(frontRight))
{
    frontRight = prompt("You did not enter a valid input. What is the pressure of the front right tire?");
}
//gathering input
var backLeft = prompt("What is the pressure of the back left tire? ");

//validating user input
while (backLeft === "" || isNaN(backLeft))
{
    backLeft = prompt("You did not enter a valid input. What is the pressure of the back left tire?");
}
//gathering input
var backRight = prompt("What is the pressure of the back right tire? ");

//validating user input
while (backRight === "" || isNaN(backRight))
{
    backRight = prompt("You did not enter a valid input. What is the pressure of the back right tire?");
}
//using array values, I think because it was specified in the assignment
var pressureArray = [frontLeft, frontRight, backLeft, backRight];

alert("The front left tire pressure is " + pressureArray[0] + ". The front right tire pressure is " + pressureArray[1] + ". The back left tire pressure is " + pressureArray[2] + ". The back right tire pressure is " + pressureArray[03] + ".");

//comparing array values and providing appropriate output
if(pressureArray[0] === pressureArray[1] && pressureArray[2] === pressureArray[3])
{
    alert("Your tires pass spec!");
}
else
{
    alert("Get your tires checked out!");
}

//labeling the section
alert("Welcome to Problem 2, Movie Ticket Price! ");

//prompting for user input
var age = prompt("Please enter your age in number format. ");

while(age === "" || isNaN(age))
{
    age = prompt("Please enter a valid input. Enter your age in number format. ");
}
//prompting for user input
var time = prompt("What time will you be attending the movie? Enter a number between 1 and 24. ");

while(time === "" || isNaN(time) || time < 0 || time > 24)
{
    age = prompt("Please enter a valid input. Enter your age in number format. ");
}

//declaring the price variables
var regPrice = 12.00;
var disPrice = 7.00;

//setting up conditional to adjust price
//providing appropriate output to user
if(age < 10 || age > 55 || (time > 14 && time < 17))
{
    alert("You qualify for the discounted price of $" + disPrice + ".");
}
else
{
    alert("You do not qualify for any discounts, so your price is " + regPrice + ".");
}
//labeling the section
alert("Welcome to problem 3, Add The Odds Or Evens!");

var choice = prompt("I have a set of numbers. Do you want me to add the odds or evens? (Type: Odd or Even)").toLowerCase();

//declaring array of numbers
var numberArray = [52, 307, 29, 111, 60, 17, 90];
var currentArray = [];

while((choice !== "even") && (choice !== "odd"))
{
    choice = prompt("Please enter a valid input. (Type: Odd or Even)").toLowerCase();
}

if (choice === "even")
{
    for(var x = 0; x < numberArray.length; x++)
    {
        if(numberArray[x] % 2 === 0)
        {
            currentArray.push(numberArray[x]);
        }
    }
    alert("There are " + currentArray.length + " even numbers in the array. ");
}
else
{
    for(x = 0; x < numberArray.length; x++)
    {
        if(numberArray[x] % 2 !== 0)
        {
            currentArray.push(numberArray[x])
        }
    }
    alert("There are " + currentArray.length + " odd numbers in the array. ");
}

//labeling the section
alert("Welcome to Problem 4, Charge It!");

//prompting user input
var maxCred = prompt("What is your maximum credit limit? Enter a number value. ");

while(maxCred === "" || isNaN(maxCred))
{
    maxCred = prompt("You did not enter a valid input. Please enter a number value. ");
}

while(maxCred >= 0)
{
    var purchase = prompt("What is the total of this purchase? ");

    while (purchase === "" || isNaN(purchase))
    {
        purchase = prompt("You did not enter a valid input. Please enter a number value. ");
    }

    maxCred = maxCred - purchase;

    alert("Your new credit limit is $" + maxCred + " dollars. ");
}

alert("You have exceeded your credit limit by $" + maxCred + "dollars. ");