# Bachelors of Web Design and Development at Full Sail University #

This repo contains all of the work from the Bachelors of Web Design and Development Degree Program at Full Sail University.

### Master Repo ###

The contents of this repository include work from:
Scalable Data Infrastructures
Database Structures
Project and Portfolio 1
Advanced Scalable Data Infrastructures
Project and Portfolio 2

### Contribution guidelines ###

David Timmons and selected instructors have Admin level access the contents of this repository. 

### Contact ###

David Timmons
drtimmons@student.fullsail.edu
575-956-3083