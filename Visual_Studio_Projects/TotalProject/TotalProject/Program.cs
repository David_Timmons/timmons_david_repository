﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TotalProject
{
    class Program
    {
        static void Main(string[] args)
        {
            /*DavidTimmons
             * 6/14/17
             * TotalProject
             * SDI Section 03
             * 
             */

            //Calculate the area and perimeter of a rectangle from user prompts
            //Tell the user what we are doing
            //Ask for width

            Console.WriteLine("Hello. We are going to find the area and perimeter of a rectangle.\r\nPlease type in a number value for the width and press return");
            string widthString = Console.ReadLine();

            //Declare a variable to hold the converted value

            double width;

            //Validate the user input using a while loop

            while (!double.TryParse(widthString, out width))
            {
                Console.WriteLine("You did not enter a number value. Please enter a number value.");
                widthString = Console.ReadLine();
            }

            //Tell the user the width and say thanks and ask for a length

            Console.WriteLine("Got it. You entered a width of {0}. \r\nPlease enter a number value for the length." ,width);
            string lengthString = Console.ReadLine();

            double length;

            while (!double.TryParse(lengthString, out length))
            {
                Console.WriteLine("You did not enter a number value. Please enter a number value.");
                lengthString = Console.ReadLine();
            }

            //Tell the user we got the length and tell them the next step
            Console.WriteLine("Thank you. You entered a width of {0} and a length of {1}.\r\nWe will now calculate the perimeter.",width,length);

            //Go create a function to calculate the perimeter
            //Print the return
            //Remember to catch the return value in a variable and use arguments

            double perimeter = CalcPeri(width, length);

            Console.WriteLine("The perimeter of the rectangle is {0}" ,perimeter);

            double area = CalcArea(width, length);

            Console.WriteLine("The area of the rectangle is {0}", area);

            

            //Tell the user we want to find the volume now
            //Ask for height

            Console.WriteLine("Lets now find the volume of the rectangular prism\r\nWhat is the height?");

            double height;

            string heightString = Console.ReadLine();

            while (!double.TryParse(heightString, out height))
            {
                Console.WriteLine("You did not enter a number value. Please enter a number value.");
                heightString = Console.ReadLine();
            }

            Console.WriteLine("You typed in a height of {0}. \r\nWe will now calculate the volume.",height);

            double volume = CalcVol(width, length, height);

            Console.WriteLine("The volume is {0}.",volume);

        }

        public static double CalcPeri(double wid, double len)
        {
            //Create a variable for perimeter
            double peri = 2 * wid + 2 * len;
            return peri;
        }

        //Create a function to calculate area

        public static double CalcArea(double w, double l)
        {
            double area = w * l;
            return area;
        }

        //Create a function to calculate volume

        public static double CalcVol(double w, double l, double h)
        {
            double volume = w * l * h;
            return volume;
        }

    }
}
