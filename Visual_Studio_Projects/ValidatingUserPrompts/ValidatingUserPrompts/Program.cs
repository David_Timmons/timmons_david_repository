﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidatingUserPrompts
{
    class Program
    {
        static void Main(string[] args)
        {
            //Validating user prompts with conditionals
            //Ask user their name

            Console.WriteLine("Hello, please type your name and press enter.");

            //Store results in variable

            string userName = Console.ReadLine();

            //Test to see if user left it blank
            //string.IsNullOrWhiteSpace(userName)
            //Returns bool true if blank, bool false if it contains anything

            if (string.IsNullOrWhiteSpace(userName))
            {
                Console.WriteLine("You need to enter a name, fuckface.");
                userName = Console.ReadLine();
            }


            Console.WriteLine("Hello "+ userName +" Thanks for running my program");
            //Console.WriteLine(string.IsNullOrWhiteSpace(userName));
            //Create a conditional to test if user input something

            Console.WriteLine("Please enter an integer between 1 and 10.");
            string userIntString = Console.ReadLine();

            //TryParse int.TryParse(variable to try to convert, out variable name to hold the converted data)

            //Declare a variable to hold the converted output value

            int userInt;
            int.TryParse(userIntString, out userInt);

            if (!(int.TryParse(userIntString, out userInt)))
            {
                Console.WriteLine("The value you entered was not an integer between 1 and 10.\r\nPlease enter an integer between 1 and 10.");
            }
            userIntString = Console.ReadLine();

            int.TryParse(userIntString, out userInt);


            int x = 1;
            while (x < 10)
            {
                x++;
            }

            Console.WriteLine(x);













        }
    }
}
