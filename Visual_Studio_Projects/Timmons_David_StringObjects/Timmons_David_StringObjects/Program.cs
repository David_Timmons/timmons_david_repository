﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timmons_David_StringObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            /*David Timmons
             * SDI Section 03
             * 06/18/17
             * String Objects Resubmission
             */
            //Prompt for the user input and validate that it is in the correct format 
            Console.WriteLine("Welcome to problem 1, Email Address Checker!\r\nPlease enter an email address.");
            string eMail = Console.ReadLine();
            //Function calling the custom function for email checker
            string verified = Checked(eMail);
            /* Test Values
            * entering test@fullsail.com provides the result "test@fullsail.com is a valid address."
            * entering test@full@sail.com provides the result "test@full@sail.com is an invalid address."
            * entering test@full sail.com provides the result "test@full sail.com is an invalid address."
            * entering 666@666.com provides the result "666@666.com is a valid address."
            */

            Console.WriteLine("\r\n\r\n");
            //labeling the section
            //Prompt for the first four values
            Console.WriteLine("Welcome To Problem 2, Seperator Swap-Out!");
            Console.WriteLine("Please enter five things.");
            string thing1 = Console.ReadLine();
            string thing2 = Console.ReadLine();
            string thing3 = Console.ReadLine();
            string thing4 = Console.ReadLine();
            string thing5 = Console.ReadLine();

            //validate that the first four values were not left blank
            while (string.IsNullOrWhiteSpace(thing1)) { Console.WriteLine("Please do not leave this blank."); thing1 = Console.ReadLine(); }
            while (string.IsNullOrWhiteSpace(thing2)) { Console.WriteLine("Please do not leave this blank."); thing2 = Console.ReadLine(); }
            while (string.IsNullOrWhiteSpace(thing3)) { Console.WriteLine("Please do not leave this blank."); thing3 = Console.ReadLine(); }
            while (string.IsNullOrWhiteSpace(thing4)) { Console.WriteLine("Please do not leave this blank."); thing4 = Console.ReadLine(); }
            while (string.IsNullOrWhiteSpace(thing5)) { Console.WriteLine("Please do not leave this blank."); thing5 = Console.ReadLine(); }

            //prompt for the first separator
            Console.WriteLine("Please enter the character that is to separate the list elements.");
            string separator1 = Console.ReadLine();

            Console.WriteLine("Please enter the new character that is to separate the list elements.");
            string separator2 = Console.ReadLine();

            ArrayList firstList = new ArrayList() { thing1, separator1, thing2, separator1, thing3, separator1, thing4, separator1, thing5 };
            ArrayList secondList = NewSeparator(thing1, thing2, thing3, thing4, thing5, separator1, separator2);
            Console.WriteLine("The list with the original separator is");foreach (string item in firstList) { Console.Write(item); }Console.WriteLine("\r\n");
            Console.WriteLine("The list with the new separator is");foreach (string item2 in secondList) { Console.Write(item2); }Console.WriteLine("\r\n");
            
        }
        //The custom method for email checker
        public static string Checked(string eMail)
        {
            while (string.IsNullOrWhiteSpace(eMail))
            {
               Console.WriteLine("Don't leave this blank.");
                eMail = Console.ReadLine();
            }
            if (!(eMail.Contains('@')))
            {
                Console.WriteLine("{0} is an invalid address.", eMail);
            }
            else if (!(eMail.Contains('.')))
            {
                Console.WriteLine("{0} is an invalid address.", eMail);
            }
            else if (eMail.Contains(' '))
            {
                Console.WriteLine("{0} is an invalid address.", eMail);
            }
            else if (eMail.LastIndexOf('@') > eMail.LastIndexOf('.'))
            {
                Console.WriteLine("{0} is an invalid address.", eMail);
            }
            else if (eMail.IndexOf('@') != eMail.LastIndexOf('@'))
            {
                Console.WriteLine("{0} is an invalid address.", eMail);
            }
            else
            {
                Console.WriteLine("{0} is a valid address.", eMail);
            }

            return eMail;

        }
        //custom method for separator swap out
        public static ArrayList NewSeparator(string thing1, string thing2, string thing3, string thing4, string thing5, string separator1, string separator2)
        {
            ArrayList secondList = new ArrayList() { thing1, separator2, thing2, separator2, thing3, separator2, thing4, separator2, thing5};
            foreach (string item in secondList)
            { item.Replace(separator1, separator2); }
            
            return secondList;
        }
            /*Test Values
             * Entering 12345, "," and "-" gives the result the original list is 1,2,3,4,5 and the new list is 1-2-3-4-5
             * entering red blue green pink, ":", and "," gives the result the original list is red:blue:green:pink and the new list is red, blue, green, pink
             * entering 6 6 6, ";" and "," gives the result the original list is 6;6;6 and the new list is 6,6,6.
             */

           
        

     






    }
}
