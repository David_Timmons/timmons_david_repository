﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VariableConversions
{
    class Program
    {
        static void Main(string[] args)
        {
            //Implicit Conversions
            //Smaller Datatype to a larger one

            short num = 23456;
            Console.WriteLine(num);


            //Implicityly Convert
            int bigNum = num;
            Console.WriteLine(bigNum);

            //Explicit Conversions require a cast operator (datatype)
            //Information Might Be Lost!!!!
            //Conversions from larger to smaller datatypes

            //WATCH FOR DATA LOSS!!!!!!

            //Convert a double to an integer

            double x = 1234.56;
            Console.WriteLine(x);

            //convert to int

            int xConverted = (int)x;
            Console.WriteLine(xConverted);


            //Convert a large number to an sbyte

            int z = 130;
            sbyte zConverted = (sbyte)z;
            Console.WriteLine(z);
            Console.WriteLine(zConverted);

            //Explicit casting takes remainder and loops it back through in the opposite way. 
            //130 into -128 (max neg sbyte value) leaves three left over.
            //128+1+1 = 126

            //Conversion Helper Classes
            //Convert Class
            //Parse Methods of built in numeric classes

            //Convert Class

            string stringValue = "56";

            //try to * by 2
            //This does not work
            //int multiplied = stringValue * 2;
            //Console.WriteLine(multiplied);

            //Convert string to number then multiply
            int multiplied = Convert.ToInt32(stringValue);
            multiplied = multiplied * 2;

            //parse 
            //Converts a string version into a different datatype

            //good for prompting the user!

            string salary = "150000";

            //parse the string and pull out an integer


            int salaryInt = int.Parse(salary);
            Console.WriteLine(salaryInt);

            //divide by 4

            salaryInt = salaryInt / 4; 

            Console.WriteLine(salaryInt);


        



                
        }
    }
}
