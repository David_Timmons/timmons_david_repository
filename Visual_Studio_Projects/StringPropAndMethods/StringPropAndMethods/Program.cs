﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringPropAndMethods
{
    class Program
    {
        static void Main(string[] args)
        {

            //String Properties and methods

            //Create a string variable

            string food = "Cheese Pizza";

            //string property of length
            //will tell you how many characters make up a string

            Console.WriteLine(food.Length);

            //Even the spaces of text strings count toward the length of a string.Length!!!

            //string methods
            //Google c# string methods, microsoft.com, link in assignment

            //string.IndexOf returns the character of an index value of a string
            //If it cannot find an index value, returns value of -1

            //searchValue gives the index value of the character searched

            //fromIndex is an optional parameter indicating where to start

            //int myReturn = string.IndexOf(searchValue, [fromIndex});

            //IndexOf()

            Console.WriteLine(food.IndexOf("pizza", 8));

            string sentence = "madam i'm adam";
            int firstAdam = sentence.IndexOf("adam", 2);
            Console.WriteLine("Adam first occurs at index {0}", firstAdam);

            //Lastindexof()

            int last = sentence.LastIndexOf("am");
            Console.WriteLine(last);

            //Substring()

            string ultimateAnswer = "Life, The Universe, and Everything.";

            //find the first comma and take everything after

            int firstComma = ultimateAnswer.IndexOf(",");
            Console.WriteLine(firstComma);

            string theSubString = ultimateAnswer.Substring(firstComma + 2,12);
            Console.WriteLine(theSubString);

            //Trim()
            //Does not change the original string

            string groceryList = "Apple, Banana, Strawberry";
            Console.WriteLine(groceryList.Trim());


            //find the index of the first comma
            int firstCommaGroc = groceryList.IndexOf(",")+1;
            Console.WriteLine(firstCommaGroc);

            //find the next/last comma
            int lastCommaGroc = groceryList.LastIndexOf(",");

            //substring of grocerylist

            string shortList = groceryList.Substring(firstCommaGroc).Trim();
            Console.WriteLine(shortList);








        }
    }
}
