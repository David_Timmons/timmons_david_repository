﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create our first Box
            //Call the constructor function
            Box firstBox = new Box(5,5,5,"Red");

            Box secondBox = new Box(6,7,8,"Blue");

            //Can we access the member variables directly?
            //Console.WriteLine(firstBox);

            //Access the information about the box
            //Getter method
            //Create this in the custom class

            //Use all of our getter methods
            Console.WriteLine("The length is {0}",firstBox.GetLength());
            Console.WriteLine("The width is {0}",firstBox.GetWidth());
            Console.WriteLine("The height is {0}",firstBox.GetHeight());
            Console.WriteLine("The color is {0}",firstBox.GetColor());
            Console.WriteLine("The length is {0}", secondBox.GetLength());
            Console.WriteLine("The width is {0}", secondBox.GetWidth());
            Console.WriteLine("The height is {0}", secondBox.GetHeight());
            Console.WriteLine("The color is {0}", secondBox.GetColor());

        }
    }
}
