﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomClass
{
    class Box
    {
        //Properties of the box go here
        //Member Variable
        //Globabl variable, only for this class
        //Member variables are all private by default
        int mLength;
        int mWidth;
        int mHeight;
        string mColor;

        //Create constructor function
        //Add parameters for the memvber variables
        public Box(int _length, int _width, int _height, string _color)
        {
            //Set the values of the memver variables using the parameters
            mLength = _length;
            mWidth = _width;
            mHeight = _height;
            mColor = _color;
        }

        //Create a getter method for the height of the cube
        //Getters should always be public
        public int GetLength()
        {
            //Return the value of height
            return mLength;
        }
        //Create getter methods for each variable
        public int GetWidth()
        {
            //Return the value of height
            return mWidth;
        }
        public int GetHeight()
        {
            //Return the value of height
            return mHeight;
        }
        public string GetColor()
        {
            //Return the value of height
            return mColor;
        }
    }
}
