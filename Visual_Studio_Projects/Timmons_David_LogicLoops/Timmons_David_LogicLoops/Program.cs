﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timmons_David_LogicLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            /*David Timmons
             * SDI Section 03
             * 06/08/17
             * Logic Loops
             */

            //Labeling the section
            Console.WriteLine("Welcome To Problem One, Tire Pressure!");

            //Prompting the user inputs
            Console.WriteLine("What is the pressure of the front left tire?");
            string firstString = Console.ReadLine();
            //validating that the user input has a value, and telling the machine what to do if it is left empty
            while (string.IsNullOrWhiteSpace(firstString))
            {
                Console.WriteLine("Please do not leave this field blank.");
                firstString = Console.ReadLine();
            }
            //validating that the user input was in the correct format with while loop
            //declaring a variable to hold the converted value
            //using try parse to tell the computer what to do if and if not the user input is in a convertible datatype
            double firstDouble;
            while (!double.TryParse(firstString, out firstDouble))
            {
                Console.WriteLine("You did not enter a number value. Please enter a number value.");
                firstString = Console.ReadLine();
            }
            //prompting for the second user input 
            Console.WriteLine("What is the pressure of the front right tire?");
            string secondString = Console.ReadLine();
            //validating that the user input was not empty with while loop
            while (string.IsNullOrWhiteSpace(secondString))
            {
                Console.WriteLine("Please do not leave this field blank.");
                secondString = Console.ReadLine();
            }
            //validating that the user input was in the correct format with while loop
            double secondDouble;
            while (!double.TryParse(secondString, out secondDouble))
            {
                Console.WriteLine("You did not enter a number value. Please enter a number value.");
                secondString = Console.ReadLine();
            }
            //prompting for the third user input
            Console.WriteLine("What is the pressure of the rear left tire?");
            string thirdString = Console.ReadLine();
            //validating that the user input was not empty with while loop
            while (string.IsNullOrWhiteSpace(thirdString))
            {
                Console.WriteLine("Please do not leave this field blank.");
                thirdString = Console.ReadLine();
            }
            //validating that the user input was in the correct format with while loop
            double thirdDouble;
            while (!double.TryParse(thirdString, out thirdDouble))
            {
                Console.WriteLine("You did not enter a number value. Please enter a number value.");
                thirdString = Console.ReadLine();
            }
            //prompting for fourth user input
            Console.WriteLine("What is the pressure of the rear right tire?");
            string fourthString = Console.ReadLine();

            //validating that the user input was not empty with while loop
            while (string.IsNullOrWhiteSpace(fourthString))
            {
                Console.WriteLine("Please do not leave this field blank.");
                fourthString = Console.ReadLine();
            }
            //validating that the user input was in the correct format with while loop
            double fourthDouble;
            while (!double.TryParse(fourthString, out fourthDouble))
            {
                Console.WriteLine("You did not enter a number value. Please enter a number value.");
                fourthString = Console.ReadLine();
            }
            double[] tireArray = new double[4] { firstDouble, secondDouble, thirdDouble, fourthDouble };

            Console.WriteLine("The front left tires pressure is {0} psi. \r\nThe front right tires pressure is {1} psi. \r\nThe rear left tires pressure is {2} psi. \r\nThe rear right tires pressure is {3} psi. ", tireArray[0], tireArray[1], tireArray[2], tireArray[3]);

            //Creating a single conditional that would determine if a given cars tires are up to spec
            //Making sure to use array value inside of conditional
            if ((tireArray[0] == tireArray[1]) && (tireArray[2] == tireArray[3]))
            {
                Console.WriteLine("Your tires pass spec!");
            }
            else { Console.WriteLine("Get your tires checked out!"); }

            /*Test Values
             * Entering values of 32, 32, 30 and 30 produces the result "Your tires pass spec!"
             * Entering values of 36, 32, 25 and 25 produces the result "Get your tires checked out!"
             * Entering Values of 166.5, 166.5, 166.5 166.5 produces the result "Your tires pass spec!"
             */

            Console.WriteLine("\r\n\r\n");

            //Labeling the section
            Console.WriteLine("Welcome To Problem Two, Movie Ticket Price!");

            //Prompting the user for the inputs

            Console.WriteLine("Please enter your age in whole number format");
            string userAge = Console.ReadLine();
            //validating that the user input has a value, and telling the machine what to do if it is left empty
            while (string.IsNullOrWhiteSpace(userAge))
            {
                Console.WriteLine("Please do not leave this field blank.");
                userAge = Console.ReadLine();
            }
            //validating that the user input was in the correct format with while loop
            //declaring a variable to hold the converted value
            //using try parse to tell the computer what to do if and if not the user input is in a convertible datatype
            int userAgeInt;
            while (!int.TryParse(userAge, out userAgeInt))
            {
                Console.WriteLine("You did not enter a whole number value. Please enter a whole number value.");
                userAge = Console.ReadLine();
            }
            Console.WriteLine("What time will you be attending the movie? Please enter a whole number value between 0 and 24");
            string movieTimeString = Console.ReadLine();
            //validating that the user input was not blank with while loop
            while (string.IsNullOrWhiteSpace(movieTimeString))
            {
                Console.WriteLine("Please do not leave this field blank.");
                movieTimeString = Console.ReadLine();
            }
            //validating that the user input was in the correct format with while loop
            int movieTimeInt;
            while (!int.TryParse(movieTimeString, out movieTimeInt))
            {
                Console.WriteLine("You did not enter a whole number value between 0 and 24. Please enter a whole number value between 0 and 24.");
                movieTimeString = Console.ReadLine();
            }
            //validating that the user input was of the correct value with if conditional
            if (movieTimeInt > 24)
            {
                Console.WriteLine("You did not enter a whole number value between 0 and 24. Please enter a whole number value between 0 and 24.");
                movieTimeString = Console.ReadLine();
            }

            //Declaring the variables to hold the price of the movie ticket, based on certain conditions
            decimal amountPaid;
            decimal regPrice = 12.00m;
            decimal disPrice = 7.00m;

            //if the user is under ten years old OR over 55 years old, OR the movie time is more than 14 AND less than 17
            //the user qualifies for the discounted price
            //else the user pays the full price
            if ((userAgeInt < 10) || (userAgeInt > 55) || ((movieTimeInt > 14) && (movieTimeInt < 17)))
            {
                amountPaid = disPrice;
                Console.WriteLine("You qualify for the discounted price of ${0}.", disPrice);
            }
            else
            {
                amountPaid = regPrice;
                Console.WriteLine("You do not qualify for any discounts, so your price is ${0}.", regPrice);
            }

            /* Test Values
             * Entering an age of 57 and a time of 20 provides the result "You qualify for the discounted price of $7.00."
             * Entering an age 9 and a time of 20 provides the result "You qualify for the discounted price of $7.00."
             * Entering an age of 38 and a time of 20 provides the result "You do not qualify for any discounts, so your price is $12.00"
             * Entering an age of 25 and a time of 16 provides the result "You qualify for the discounted price of $7.00."
             * Entering an age of 666 and a time of 13 provides the result "You qualify for the discounted price of $7.00."
             */

            Console.WriteLine("\r\n");

            //In my console, this code outputs each iteration of adding the even or odd numbers
            //I checked the rubric thoroughly and there's nothing in there about this problem only having one output
            //Everything other than that runs correctly, and the math that it outputs is accurate
            //So, here's hoping that I don't lose too many points for that :)


            //Labeling the section
            Console.WriteLine("Welcome To Problem 3, Add Up The Odds Or Evens!");
            //Prompting the user inputs
            Console.WriteLine("I have a set of numbers. Do you want me to add the odd numbers or the even numbers?\r\nPlease enter the words odd or even in all lowercase letters.");
            string oddOrEven = Console.ReadLine();

            //validating that the user input has a value, and telling the machine what to do if it is left empty
            while (string.IsNullOrWhiteSpace(oddOrEven))
            {
                Console.WriteLine("Please do not leave this field blank.");
                oddOrEven = Console.ReadLine();
            }
            //validating that the user input was in the correct format with a while loop
            while (!((oddOrEven == "odd") || (oddOrEven == "even")))
            {
                Console.WriteLine("You did not enter the words odd or even in all lowercase letters. Please try again.");
                oddOrEven = Console.ReadLine();
            }
            //Creating said set of numbers
            int[] numberArray = new int[7];
            numberArray[0] = 52;
            numberArray[1] = 307;
            numberArray[2] = 29;
            numberArray[3] = 111;
            numberArray[4] = 60;
            numberArray[5] = 17;
            numberArray[6] = 90;

            //Creating variables to hold a new value
            int numberEven = 0;
            int numberOdd = 0;

            //Creating the loop to provide the output
            foreach (int number in numberArray)
            {
                // if oddOrEven is "even" AND the number in the array can be divided by 2 with no remainder
                if ((oddOrEven == "even") && (number % 2 == 0))
                {
                    //numberEven = 0 + the first number that is even, plus the second number that is even and so on
                    //Here is where my iterations printing out multiple times happens
                    numberEven = numberEven + number;
                    Console.WriteLine("The total of all the even numbers is {0}", numberEven);
                }
                //  if oddOrEven is "odd" AND the number in the array cannot be divided by 2 with no remainder
                else if ((oddOrEven == "odd") && (number % 2 != 0))
                {
                    //numberOdd = 0 + the first number that is odd, plus the second number that is odd and so on
                    //Again with printing out each iteration
                    numberOdd = numberOdd + number;
                    Console.WriteLine("The total of all the odd numbers is {0}", numberOdd);
                }

            }

            /* Test Values
             * Entering even for test value array 1 gives "The total of all the even numbers is 12."
             * Entering odd for test value array 1 gives "The total of all the odd numbers is 16." 
             * Entering even for test value array 2 gives "The total of all the even numbers is 42."
             * Entering odd for test value array 2 gives "The total of all the odd numbers is 45."
             * Entering even for my test value array gives "The total of all the even numbers is 202"
             * Entering odd for my test value array gives "The total of all the odd numbers is 464."
             */

            Console.WriteLine("\r\n\r\n");

            //Labeling the section
            Console.WriteLine("Welcome To Problem 4, Charge It!");

            //Prompting user input
            Console.WriteLine("What is your maximum credit limit?\r\nPlease enter a number value in decimal format.");
            string creditString = Console.ReadLine();
            //Validating that the user input is not blank
            while (string.IsNullOrWhiteSpace(creditString))
            {
                Console.WriteLine("Please do not leave this field blank.");
                creditString = Console.ReadLine();
            }
            //Creating a new variable to hold converted value
            //Validating that the user input was in the correct format
            decimal creditDecimal;
            while (!(decimal.TryParse(creditString, out creditDecimal)))
            {
                Console.WriteLine("You did not enter a number value in decimal format. \r\nPlease enter a number value in decimal format.");
                creditString = Console.ReadLine();
            }
            //Prompting more user input
            Console.WriteLine("What is the total of this purchase?\r\nPlease enter a value in decimal format.");
            string purchaseString = Console.ReadLine();
            //Validating that the user input is not blank

            while (string.IsNullOrWhiteSpace(purchaseString))
            {
                Console.WriteLine("Please do not leave this field blank.");
                purchaseString = Console.ReadLine();
            }

            //declaring variables to use later
            decimal purchaseDecimal = 0;
            decimal difference = 0;
            decimal totalPurchase = 0;

            decimal overage = 0;

            // while the remaining balance after each purchase is greater than 0
            while (difference >= 0) 
            {
                //trying to convert and telling the user what to do if couldn't
                while (!decimal.TryParse(purchaseString, out purchaseDecimal))
                {
                    Console.WriteLine("You did not enter a number value in decimal format. \r\nPlease enter a number value in decimal format.");
                    purchaseString = Console.ReadLine();
                }
                //the amount the purchase went past 0 is 0 plus the remaining balance - the purchase
                overage = 0 + difference - purchaseDecimal;
                //as long as the remaining balance minus the total of all the purchases is equal to or greater than 0
                if (difference - totalPurchase >= 0)
                {
                    //the total of all the purchases is each purchase plus the next purchase
                    //the remaining balance is the max credit minus the total of all the purchases
                    //prompting for next input
                    totalPurchase += purchaseDecimal;
                    difference = creditDecimal - totalPurchase;
                    Console.WriteLine("After your purchase of ${0} you have ${1} remaining in your balance",purchaseDecimal, difference);
                    Console.WriteLine("What is the total of this purchase?");
                    purchaseString = Console.ReadLine();


                }
                else
                {
                    //Prompting the user for what happened
                    Console.WriteLine("You have exceeded your credit limit by ${0}.", overage);
                    //Changing the value of difference to satisfy the exit condition for the main while loop
                    difference = difference + overage;
                    string exit = Console.ReadLine();


                }

                /* Test Values
                 * Entering a value of 20 for max credit, 5 for purchase 1, 12 for purchase 2, and 7 for purchase 3 provides the result "You have exceeded your credit limit by $-4.00
                 * Entering a value 100.00 for max credit, 33.00 for purchase 1, 33.00 for purchase 2, and 700.00 for purchase 3 provides the result "You have exceeded your credit limit by $-666.00 
                 */




                






























            }





        }
        
    }
}
