﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicBookProject
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create an instance of our comic book class
            //Creates an object with all of the info attached to it
            ComicBook Avengers = new ComicBook("Avengers Annual #16", 1987, 1.25m, 4.00m);

            Console.WriteLine("The title of the comic book is {0}", Avengers.GetTitle());
            Console.WriteLine("The year it was released was {0}", Avengers.GetYearPublished());
            Console.WriteLine("The original cost was {0}", Avengers.GetOriginalCost());
            Console.WriteLine("The current value is {0}",Avengers.GetCurrentValue());

            //Change the current value of this book.
            Avengers.SetCurrentValue(20.00m);

            Console.WriteLine("The current value after the setter is {0}", Avengers.GetCurrentValue());

            //Create a second comic book

            ComicBook Spiderman = new ComicBook("The amazing spider #194", 1979, .40m, 2000.00m);
            Console.WriteLine("\r\nThe title of the comic book is {0}", Spiderman.GetTitle());
            Console.WriteLine("The year it was released was {0}", Spiderman.GetYearPublished());
            Console.WriteLine("The original cost was {0}", Spiderman.GetOriginalCost());
            Console.WriteLine("The current value is {0}", Spiderman.GetCurrentValue());

            //Find out the net worth of the spiderman comic

            Console.WriteLine("\r\nThe net worth of the Spiderman comic is {0}",Spiderman.NetWorth());
            Spiderman.SetCurrentValue(100.00m);
            Console.WriteLine("The net worth of the Spiderman comic after the setter is {0}", Spiderman.NetWorth());

            Console.WriteLine("\r\nThe avengers comic is {0}",Avengers.Age(2017));
            Console.WriteLine("The spiderman comic is {0}", Spiderman.Age(2017));
        }
    }
}
