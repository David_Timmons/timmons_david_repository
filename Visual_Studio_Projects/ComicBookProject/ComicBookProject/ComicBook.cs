﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicBookProject
{
    class ComicBook
    {
        //Create memver variables
        //Describe the comic book
        string mTitle;
        int mYearPublished;
        decimal mOriginalCost;
        decimal mCurrentValue;

        //Create the constructor function
        public ComicBook(string _title, int _yearPublished, decimal _orignalCost, decimal _currentValue)
        {
            //Use the incoming parameters to initialize our original member variables
            mTitle = _title;
            mYearPublished = _yearPublished;
            mOriginalCost = _orignalCost;
            mCurrentValue = _currentValue;
        }

        //Getters
        //Returns the information back to where it was called
        public string GetTitle()
        {
            //returning the value of title
            return mTitle;
        }
        public int GetYearPublished()
        {
            return mYearPublished;
        }
        public decimal GetOriginalCost()
        {
            return mOriginalCost;
        }
        public decimal GetCurrentValue()
        {
            return mCurrentValue;
        }
        //Setters change the member variables
        public void SetTitle(string _title)
        {
            //Change the member variable and use the parameter
            mTitle = _title;
        }
        public void SetYearPublished(int _yearPublished)
        {
            mYearPublished = _yearPublished;
        }
        public void SetOriginalCost(decimal _originalCost)
        {
            mOriginalCost = _originalCost;
        }
        public void SetCurrentValue(decimal _currentValue)
        {
            mCurrentValue = _currentValue;
        }

        //Create a custom method
        //Calculate our net worth
        //Take current value and subract original cost
        //Return it as a formatted text string

        public string NetWorth()
        {
            //Calculate the net worth
            decimal netWorth = mCurrentValue - mOriginalCost;
            //Convert the decimal to formatted text string
            string netWorthFormatted = netWorth.ToString("C");
            return netWorthFormatted;
        }
        //Create a method that tells us how old the comic book is
        //Take in an argument of the current year

        public int Age(int _currentYear)
        {
            int age = _currentYear - mYearPublished;
            return age;
        }

    }
}
