﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            //For Loops
            //for (variable ; test ; increment of change){}
            for (int counter=1; counter<5; counter++)
            {
                Console.WriteLine("The value of counter is {0}" ,counter);
            }

            //Create an array to loop through
            int[] myBills = new int[4] { 30, 40, 50, 60 };
            Console.WriteLine(myBills[0]);

            //Create the foreach loop

            foreach (int arrayItem in myBills)
            {
                //Each item in the array will loop
                Console.WriteLine("The item in the array is {0}. ", arrayItem);
            }


        }


    }
}
