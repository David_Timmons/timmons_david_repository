﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomcClass
{
    class Box
    {
        //Properties of the box class go here
        //member variable is the same this as a property
        int mLength;
        int mWidth;
        int mHeight;
        string mColor;

        //Create a constructor function
        public Box (int _length, int _width, int _height, string _color)
        {
            //Set the values of the member variable using parameters
            mLength = _length;
            mWidth = _width;
            mHeight = _height;
            mColor = _color;
        }
    }
}
