﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timmons_David_FinalProject
{
    class Program
    {
        static void Main(string[] args)
        {/* 
            
            David Timmons
            06/19/17
            SDI Section 03
            Final Project

          */


            //labeling the section and promting for the first user input
            Console.WriteLine("Welcome to Final Project!\r\nPlease enter a list of events you'd like to buy tickets for,\r\nwith each event separated by a comma.");
            string eventList = Console.ReadLine();
            //validating that the user input was not left blank
            while (string.IsNullOrWhiteSpace(eventList))
            {
                Console.WriteLine("Do not leave this field blank.");
                eventList = Console.ReadLine();
            }
            //creating the variables to accept return values from custom functions
            string[] events = TextToArray(eventList);
            decimal[] costs = PromptForCosts(events);
            decimal totals = SumOfCosts(costs);

            //output the 
            { Console.WriteLine("The cost of all your events will be ${0}.",Math.Round(totals, 2)); }
        }
        //Creating the custom function to accept the user input string, manipulate the data, and turn it into an array
        public static string[] TextToArray(string eventList)
        {   //creating a new variable from a set of methods on an old variable, and returning it to the function name
            string[] eventArray = eventList.Trim().Split(',').ToArray();
            return eventArray;
        }
        //Creating the custom function to accept the user input for the costs, validate and convert it
        public static decimal[] PromptForCosts(string[] events)
        {   //creating a new array with the parameter that it is to be the same length as the array created in the preceding function
            decimal[] costArray = new decimal[events.Length];
            //creating the for loop to validate that the user input was not blank
            for (int i = 0; i < events.Length; i++)
            {
                Console.WriteLine("How much did this ticket cost?");
                string costString = Console.ReadLine();
                //creating the variable to accept the converted data
                decimal cost;
                //creating the while loop to validate the user input was in the correct format and convert it
                while (!decimal.TryParse(costString, out cost))
                {
                    Console.WriteLine("Please enter a number value.");
                    costString = Console.ReadLine();
                }
                //using a loop to cycle through the new array and assign the user input to the corresponding indexes
                foreach (decimal item in costArray)
                { costArray[i] = cost; }
            }
            //returning the newly created array to the function name
            return costArray;
            }
        //creating the custom function to calculate the sum of all the elements in the decimal array
        public static decimal SumOfCosts(decimal[] costs)
        {   //creating the variable to accept data input from the decimal array
            decimal total = costs.Sum();
            //returning the sum of all the elements in the decimal array to the function name
            return total;
        }
        /* Test Values
         * Entering the list Fiddler on the Roof, Wonderwoman, and Guardians of the Galaxy 2,
         * And the corresponding values 50.00, 12.00 and 7.00 provides the result "The cost of all your events will be $77.00."
         * Entering "Twelve dollars" re-prompts the user for a number value.
         * Entering the list Enter the Wu-Tang 36 Chambers, Big Pun Capital Punishment, Nas Illmatic, Bronze Nazareth The Great Migration, Blackstar
         * And the corresponding values 15.00, 15.00, 15.00 15.00 and 1.123456789 provides the result "The cost of all your events will be $61.12."
         */
    }
}
