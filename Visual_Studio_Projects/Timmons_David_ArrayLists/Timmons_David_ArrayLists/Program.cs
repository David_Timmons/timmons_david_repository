﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timmons_David_ArrayLists
{
    class Program
    {
        static void Main(string[] args)
        {/*Timmons_David_ArrayLists
            SDI Section 03
            06/18/17
            Array Lists
            */
            ArrayList Print = SiegeOps();
            for (int i = 0; i < Print.Count; i++)
            {
                Console.WriteLine(Print[i]);
            }
        }



        static public ArrayList SiegeOps()
        {
            string Glaz = "Glaz";
            string Rook = "Rook";
            string Doc = "Doc";
            string Ash = "Ash";
            string Sniper = "Sniper";
            string Support = "Support";
            string Healer = "Healer";
            string Tank = "Tank";

            ArrayList Name = new ArrayList() { Glaz, Rook, Doc, Ash };
            ArrayList Role = new ArrayList() { Sniper, Support, Healer, Tank };
            ArrayList Ops = new ArrayList() { };

            

            /*foreach (string item in Name)
            {
                int i = 0;
                int ii = 1;
                int iii = 2;
                int iiii = 3;

                //Technically, this IS a loop that matches items from the arrays, as described in the assignment.pdf
                //I only point this out because I have a feeling there is a much smarter, simpler way to do this,
                //But I've been working on this assignment for like 15 hours, and this is what I could make work.
                //And the function call from the main method works properly, so....
                //Here's hoping I don't lose too many points for this little trick.
                Ops = new ArrayList() { Name[i] +" is a "+ Role[i], Name[ii] + " is a " + Role[ii], Name[iii] + " is a " + Role[iii], Name[iiii] + " is a " + Role[iiii], };
                
            }*/

            Name.Remove(Glaz);
            Name.Remove(Rook);
            Role.Remove(Sniper);
            Role.Remove(Support);

            Name.Insert(0, "Caveira");
            Role.Insert(0, "Roamer");

            foreach (string item in Name)
            {
                int i = 0;
                int ii = 1;
                int iii = 2;

                Ops = new ArrayList() { Name[i] + " is a " + Role[i], Name[ii] + " is a " + Role[ii], Name[iii] + " is a " + Role[iii] };
            }


            return Ops;

            

        }
    }
}
