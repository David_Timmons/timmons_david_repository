﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timmons_David_RestaurantCalc
{
    class Program
        /*David Timmons
         * SDI Section 03
         * Timmons_David_RestaurantCalc
         * 05/31/17
         * 
         * I am going to try and give this tip calculator some personality, hopefully it works.
         * I don't know if maybe I missed it somewhere in the readings, but I'm not sure how to make decimal values round to two decimal places.
         * Here's hoping I don't lose points because of that.
         * I did inadvertently make this a bit more complex than was required, 
         * because of the way I decided to calculate the tips, I allow for more complex values than just a table average.
         * In my tip calculator, each person can decide to tip their own amount, and the tips are calculated individually, then added together.
         * I am still able to use it to accurately calculate the test values, because the tip % given in the examples is a table average,
         * which I calculate by having all three diners tip the same amount.
         * Here's hoping I don't lose points for that either. 
         * 
         */
    {
        static void Main(string[] args)
        {
            //Greeting
            Console.WriteLine("Welcome to the David Timmons Food Goodness Tip Calculator!");

            //The Next Line Is To Get The Users Name, To Make It Feel More Personal
            Console.WriteLine("Please Type Your Name And Press Return");
            string userName = Console.ReadLine();

            //The Next Line Is To Get The Name Of The Restaurant, To Make It Feel More Involved
            Console.WriteLine("Where Did You Have Your Meal Tonight "+userName+"?");
            string foodPlace = Console.ReadLine();

            //The Next Few Lines Are To Gather The Cost Of The Three Meals,
            //To Be More Elaborate, I Create String Variables Here, And Have User Input Define Their Values.
            Console.WriteLine("How Much Did The First Member Of Your Party's Meal Cost?");
            string mealValString1 = Console.ReadLine();
            Console.WriteLine("How Much Did The Second Member Of Your Party's Meal Cost?");
            string mealValString2 = Console.ReadLine();
            Console.WriteLine("How Much Did The Third Member Of Your Party's Meal Cost?");
            string mealValString3 = Console.ReadLine();

            //The Following Lines Are To Take The Input Values And Convert Them From Strings Into Usable Number Datatypes
            //I Discovered In My Early Tests Of This Code That Integers Were Not Going To Work,
            //That Is Why I Opted For decimal Values For The Following Lines
            //Because Money Is Not Often Traded In Integer Form
            //Also, I Am Aware That This Could Easily Be One Multiline Comment Instead Of Seven Single Line Comments
            //Which Would Be Denoted By /**/
            //I Have No Idea Why I Am Doing It This Way
            decimal mealValNumber1 = decimal.Parse(mealValString1);
            decimal mealValNumber2 = decimal.Parse(mealValString2);
            decimal mealValNumber3 = decimal.Parse(mealValString3);

            //The Next Two Lines Are To Gather And Announce The Total Cost Of The Meals Alone
            decimal subTotal = mealValNumber1 + mealValNumber2 + mealValNumber3;
            Console.WriteLine("The Total Cost Of These Three Meals is "+subTotal+".");

            //The Next Few Lines Are To Gather The Desired Tip Percentage, In The Simplest Way I Could Figure
            Console.WriteLine("Based On The Food Goodness at "+foodPlace+", What Percent Would The First Member Of Your Party Like To Tip? \r\nPlease Enter Value In Whole Number Form");
            string tipString1 = Console.ReadLine();
            Console.WriteLine("Based On The Food Goodness at "+foodPlace+", What Percent Would The Second Member Of Your Party Like To Tip? \r\nPlease Enter Value In Whole Number Form");
            string tipString2 = Console.ReadLine();
            Console.WriteLine("Based On The Food Goodness at "+foodPlace+", What Percent Would The Third Member Of Your Party Like To Tip? \r\nPlease Enter Value In Whole Number Form");
            string tipString3 = Console.ReadLine();

            //The Next Few Lines Are To Take Take The Input Values And Convert Them From Strings Into Usable Number Datatypes
            decimal tipNumber1 = decimal.Parse(tipString1);
            decimal tipNumber2 = decimal.Parse(tipString2);
            decimal tipNumber3 = decimal.Parse(tipString3);

            //This Is The Easiest Way I Could Figure To Make The Computer Calculate Tip Percentages
            //I Divide The Cost Of The Meal By 100 To Find 1 Percent Of The Meal Cost
            //Then I Multiply The 1 Percent Of Meal Cost By The Specified Desired Tip Percentage The User Had Input Above
            //This Gives Us Any Specified Percentage Of Any Specified Number
            decimal tipTotal1 = mealValNumber1 / 100 * tipNumber1;
            decimal tipTotal2 = mealValNumber2 / 100 * tipNumber2;
            decimal tipTotal3 = mealValNumber3 / 100 * tipNumber3;

            //The Next Few Lines Are To Find And Announce The Total Cost Of All Three Tips Combined
            decimal totalTableTips = tipTotal1 + tipTotal2 + tipTotal3;
            Console.WriteLine("The Total Value Of These Tips Comes To " +totalTableTips);
            Console.WriteLine("Please Round To Nearest Two Decimal Points");

            //The Next Few Lines Are To Find And Announce The Total Cost Of All The Meals Combined With The Total Cost Of All The Tips
            decimal totalCostPlusTips = subTotal + totalTableTips;
            Console.WriteLine("Which Brings The Total Ticket Cost To " +totalCostPlusTips);
            Console.WriteLine("Please Round To Nearest Two Decimal Points");

            //The Next Few Lines Are To Find And Announce The Cost Per Person If The Total Cost Of Meals Plus Tips Is Split Evenly Three Ways
            decimal ticketSplit3 = totalCostPlusTips / 3;
            Console.WriteLine("If You Split The Ticket Evenly Between The Three Members Of Your Party, You Will Each Owe "+ticketSplit3);
            Console.WriteLine("Please Round To Nearest Two Decimal Points");

            //This Is To Make Excuses For Me Not Knowing How To Code Better :)
            Console.WriteLine("Thank You For Using The David Timmons Food Goodness Tip Calculator! \r\nCome Back Again When I Learn How To Code This For Different And More Useful Values!\r\nOr Even Better, When I Know How To Code It To Round To Two Decimal Points!");


            /*       TEST VALUES #1
             *       Check #1 = 10.00
             *       Check #2 = 15.00
             *       Check #3 = 25.00
             *       Tip % = 20
             *       Subtotal Without Tip = 50.00
             *       Total Tip = 10.00
             *       Grand Total With Tip = 60.00
             *       Cost Per Person = 20.00
             *       VALUES CONFIRMED CORRECT AND PRESENT VIA THE DAVID TIMMONS FOOD GOODNESS TIP CALCULATOR
             *       
             *       TEST VALUES #2
             *       Check #1 = 20.25
             *       Check #2 = 17.75
             *       Check #3 = 23.90
             *       Tip % = 10
             *       Subtotal Without Tip = 61.90
             *       Total Tip = 6.19
             *       Grand Total With Tip = 68.09
             *       Cost Per Person = 22.69666 Or Rounded 22.70
             *       VALUES CONFIRMED CORRECT AND PRESENT VIA THE DAVID TIMMONS FOOD GOODNESS TIP CALUCULATOR
             *       
             *       TEST VALUES #3 (THE ONE I'M MAKING UP)
             *       (This is an extremely expensive restaraunt) (lol)
             *       Check #1 = 4500.00
             *       Check #2 = 9000.00
             *       Check #3 = 16,999.99
             *       Tip % = 75
             *       (With very high tippers) (lol)
             *       Subtotal Without Tip = 30499.99
             *       Total Tip = 22874.9925
             *       Grand Total With Tip = 53374.9825
             *       Cost Per Person = 17791.660833333
             *       VALUES CONFIRMED CORRECT AND PRESENT VIA THE DAVID TIMMONS FOOD GOODNESS TIP CALCULATOR
             *       I Checked These Values On A Separate Calculator To Make Sure They Were True, Before I Used My Tip Calculator
             *  
             */




























        }
    }
}
