﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timmons_David_CountTheFish
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             *David Timmons
             *SDI Secion 03
             *06/11/17
             *Count The Fish
             */

            //THIS IS CURRENTLY SETUP FOR THE TEST VALUES AT THE END OF THE ASSIGNMENT,
            //IN WHICH I ADDED 3 EXTRA RED FISH TO THE END OF THE ORIGINAL ARRAY.

            Console.WriteLine("Welcome to count the fish!");
            //Original String Array
            string[] fish = new string[13] { "Red", "Blue", "Green", "Yellow", "Blue", "Green", "Blue", "Blue", "Red", "Green", "Red", "Red", "Red" };
            //Creating a second array that corresponds to the value of the original,
            //This is because I'll be needing to add these together later, and I can't add strings
            int[] fishInt = new int[13] { 1, 2, 3, 4, 2, 3, 2, 2, 1, 3, 1, 1, 1 };
            //Red = 1 Blue = 2 Green = 3 Yellow = 4

            //So I can visually doublecheck myself
            //Not actually here to serve a purpose other than that
            fish[0] = "Red";
            fish[1] = "Blue";
            fish[2] = "green";
            fish[3] = "Yellow";
            fish[4] = "Blue";
            fish[5] = "Green";
            fish[6] = "Blue";
            fish[7] = "Blue";
            fish[8] = "Red";
            fish[9] = "Green";
            fish[10] = "Red";
            fish[11] = "Red";
            fish[12] = "Red";

            //Prompt user input and create menu
            Console.WriteLine("What color fish do you want to count?");
            Console.WriteLine("Please Press 1 For Red.\r\nPlease Press 2 For Blue.\r\nPlease Press 3 For Green.\r\nPlease Press 4 For Yellow.");
            string colorString = Console.ReadLine();
            
            
            //Declare a new variable to hold converted value
            //Converting the variable
            //Validating that the user input was in the correct format
            //Reprompting if it was not
            int color;
            while (!int.TryParse(colorString, out color))
            {

                Console.WriteLine("You did not enter one of the specified colors. Please try again.");
                colorString = Console.ReadLine();
                
                //Validating that the user input was of the correct value
                //Reprompting if it was not
                if   (!((color == 1) || (color == 2) || (color == 3) || (color == 4)))
                     {
                     Console.WriteLine("You did not enter one of the specified colors. Please try again.");
                     colorString = Console.ReadLine();
                     }
             }
           //Declaring new variables to use in adding the array indexes later
            
            int numberRed = 0;
            int numberBlue = 0;
            int numberGreen = 0;
            int numberYellow = 0;

            //Declaring a variable to hold the color information based on a condition for output to the user
            string colorOutput;

            //Declaring a new variable to make each element in the converted array equal 1
            int fishVal = 0;
            //Making each element in the array equal 1, so that adding up the four blues, with user input 2, doesn't equal 8.
            foreach (int eachFish in fishInt)
            { fishVal = 1; }

            //Selecting the array elements based on user input and conditions
            //Defining what the machine is to do once it has selected the elements
            foreach (int arrayItem in fishInt)
            {
                
                if ((color == 1) && (arrayItem == 1))
                {
                    numberRed = numberRed + fishVal;
                }
                else if ((color == 2) && (arrayItem == 2))
                {
                    numberBlue = numberBlue + fishVal;
                }
                else if ((color == 3) && (arrayItem == 3))
                {
                    numberGreen = numberGreen + fishVal;
                }
                else if ((color == 4) && (arrayItem == 4))
                {
                    numberYellow = numberYellow + fishVal;
                }

            //Printing out the results of the selection and addition process
            }if (color == 1)
            {
                colorOutput = "Red";
                Console.WriteLine("There are {0} {1} fish in the fishtank.",numberRed, colorOutput);
            }
            else if (color == 2)
            {
                colorOutput = "Blue";
                Console.WriteLine("There are {0} {1} fish in the fishtank.",numberBlue, colorOutput);
            }
            else if (color == 3)
            {
                colorOutput = "Green";
                Console.WriteLine("There are {0} {1} fish in the fishtank",numberGreen, colorOutput);
            }
            else if (color == 4)
            {
                colorOutput = "Yellow";
                Console.WriteLine("There are {0} {1} fish in the fishtank",numberYellow, colorOutput);
            }

        /* Test Values
         * Entering an initial value of 1 for Red provides the result "There are 2 red fish in the fishtank."
         * Entering an initial value of 2 for Blue provides the result "There are 4 blue fish in the fishtank."
         * Entering an initial value of 3 for Green provides the result "There are 3 green fish in the fishtank."
         * Entering an initial value of 4 for Yellow provides the result "There are 1 yellow fish in the fishtank."
         * Entering an initial value of 6 prompts the user for a valid input. 
         * The way it is currently set up, I added 3 extra red fish, and the code still works.
         
             */
                 
                 
                 
                 





        }
    }
}
