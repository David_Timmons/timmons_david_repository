﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASD_Week_1_Review
{
    class Program
    {
        static void Main(string[] args)
        {

            
            //Variables 
            //Like a box. What is holds, Name by which you will identify the box.
            //Syntax
            //type (name);
            //(name) = value
            bool programRunning;
            // type (name) = value; 
            programRunning = true;
            bool programIsRunning = true;

            //primitive varialbles
            //bool, signed, unsigned, short, int, long, decimal, double, float, byte

            //Basic Operations
            //Math Operations
            //Parenthesis
            //Multiplication and Division
            //Addition and Subtraction

            //VS goes left to right, except in the case of order of operations
            //Modulus Operator %
            //Integer Division
            //Regular Division provides fractional portion, Integer Division does not provide remainder

            int v1 = 2;
            int v2 = 4;
            int v3 = 10;

            int result = v1 + v1;
            //String interpolation used to display the result
            Console.WriteLine($"result of {v1} + {v1} = {result}");

            result = v2 - v1;
            Console.WriteLine($"result of {v2} - {v1} = {result}");

            result = v2 * v1;
            Console.WriteLine($"result of {v2} * {v1} = {result}");

            result = v3 / v1;
            Console.WriteLine($"result of {v3} / {v1} = {result}");

            result = v2 + v1 * v3;
            Console.WriteLine($"result of {v2} + {v1} * {v3} = {result}");

            result = (v2 + v1) * v3;
            Console.WriteLine($"result of ({v2} + {v1}) * {v3} = {result}");

            result = v3 % v2;
            Console.WriteLine($"result of ({v3} % {v2}) = {result}");

            result = v3 / v2;
            Console.WriteLine($"result of {v3} / {v2} = {result}");


            //Compound operations
            //Increment and Decrement operators
            //Compound operations handle multiple pieces of data in a single line

            // *=
            // /+
            // +=
            // -+
            //variable = variable + 2 is the same as variable += 2

            //Increment and Decrement 
            // ++variable (pre);
            // variable++ (post);
            // --variable (pre);
            // variable-- (post);

            //pre gives you the new value of the variable to use in your equation
            //post increments after everything else so even if its used in a statement the value doesn't change until after the statement has executed

            float currentValue = 0;
            Console.WriteLine($"result of {currentValue} += {2} is {currentValue += 2}");
            Console.WriteLine($"result of {currentValue} -= {1} is {currentValue -= 1}");
            Console.WriteLine($"result of {currentValue} *= {10} is {currentValue *= 10}");
            Console.WriteLine($"result of {currentValue} /= {4} is {currentValue /= 4}");

            
            
            //Increment Operations
            Console.WriteLine($"pre increment ++variable on {currentValue} in a statement results in the value {++currentValue}");
            Console.WriteLine($"post increment variable++ on {currentValue} in a statement results in the value {currentValue++}");
            //Decrement Operations
            Console.WriteLine($"pre decrement --variable on {currentValue} in a statement results in the value {--currentValue}");
            Console.WriteLine($"post decrement variable-- on {currentValue} in a statement results in the value {currentValue--}");
            
            

            //Relational Operations
            //Relational Operations compare the left and right side of the operator
            // >(greater than), <(less than), ==(equal to), !(not), >=(greater than or equal to), <=(less than or equal to)
            int leftValue = 10;
            int rightValue = 5;
            int anotherValue = 10;
            bool bTrue = true;
            Console.WriteLine($"{leftValue} > {rightValue} is {leftValue > rightValue}");
            Console.WriteLine($"{leftValue} < {rightValue} is {leftValue < rightValue}");
            Console.WriteLine($"{leftValue} >= {rightValue} is {leftValue >= rightValue}");
            Console.WriteLine($"{leftValue} <= {rightValue} is {leftValue <= rightValue}");
            Console.WriteLine($"{leftValue} > {anotherValue} is {leftValue == anotherValue}");
            Console.WriteLine($"!{bTrue} is {!bTrue}");

            

            //Input and Output
            //Input  - Console.ReadLine - reads in a line of text ending when the user hits return.
            //         Console.ReadKey - reads in a single key press from the user and continue.
            Console.WriteLine("This is a sample line of output\n\n");
            Console.Write("Please enter your name: ");
            string name = Console.ReadLine();

            Console.WriteLine($"Hello {name}\n\n");
            Console.WriteLine("How old are you in years?");
            string stringAge = Console.ReadLine();
            int age = Convert.ToInt32(stringAge);
            Console.WriteLine($"{name} is {age} years old.\n\n");

          
            //Arrays
            //
            // int[] arrayOfInts = new int[size];
            // arrayOfInts = {1, 2, 3, 4, 5, 6, 7, 8};
            //
            // Multi-dimensional Arrays
            // int[,] multiArray = new int[size_1,size_2]
            //
            // Accessing the array
            // index - value placed inside the [] starts at zero and ends at length minus 1
            // array.length returns the total length of the the array
            // arrayOfInts[5] == 6;

            string[] names;
            names = new string[3];

            Console.WriteLine("Please enter three names.");
            Console.Write("Enter Name 1: ");
            names[0] = Console.ReadLine();
            Console.Write("Enter Name 2: ");
            names[1] = Console.ReadLine();
            Console.Write("Enter Name 3: ");
            names[2] = Console.ReadLine();

            Console.WriteLine($"Name 1 is {names[0]}");
            Console.WriteLine($"Name 2 is {names[1]}");
            Console.WriteLine($"Name 3 is {names[2]}");

           

            //Conditionals
            //Only have certain sections of the code run if a certain condition is met
            //
            // if ("statement that evaluates to true or false")
            // {
            //        this block only executes if the statement is true
            // }
            // whats after the block executes no matter what
            //
            // if (statement)
            //     this line only runs if the if statement was true
            // else 
            //     this line only runs if the if statement was false

            // if (statement)
            // {}
            // else if (statement)
            // {}
            // else
            // {}

            // switch (itemToCheck)
            // {
            //      case 1:
            //         code here
            //         break;
            //      case "hello":
            //         break;
            //      default:
            //          code here
            //          break;


            Console.WriteLine("Pick a number between 1 and 100: ");
            string userInput = Console.ReadLine();
            int userChoice = Convert.ToInt32(userInput);

            if (userChoice == 42)
            {
                Console.WriteLine("The answer to life the universe and everything \n\n");
            }
            else if (userChoice == 7)
            {
                Console.WriteLine("This seems like a pretty good number\n\n");
            }
            else
            {
                Console.WriteLine("You feel like there are other numbers that are more important that that one.");
            }


           

            //Logical Operators
            // 
            // Logical operators are some keywords like and and or that you can use for a part of your conditional checks if you need a more complex check
            // And - && - Both sides must evaluate to true, short circuits on the first false
            // Or - || - One side must evaluate to true, short circuits on the first true
            // Not - !
            // Grouping - ()
            /*
            Console.WriteLine("Please enter a number between 1 and 100: ");
            string userInput = Console.ReadLine();
            int userChoice = Convert.ToInt32(userInput);
            */
            if (userChoice > 0 && userChoice < 101)
            {
                Console.WriteLine("You selected a value in the desired range.\n\n");
            }
            else
            {
                Console.WriteLine("Your choice was out of bounds.\n\n");
            }

    

            // LOOPS

            // for (initializer[can be blank],condition[controls when the loop stops],incrementer[happens at the end of each iteration])
            // for (int i = 0; i < 10; i++)
            // initializer and increment can be done outside of the loop
            // for (;i < 10;)
            // {
            //      i = 0
            //      i++
            // }

            // while loop
            // while (condition)
            // {
            //    this repeats as long as condition is true
            //    while loop with true condition creates infinite loops, not good. SAD.
            // }

            // do while loop
            // do
            // {
            //     will always execute this section of code at least once
            // }while(condition)

            // foreach loop
            // foreach (int item in array)
            // {
            //     will execute this once for each item in the collection
            // }

            
            // Hide everything that was done before this line
            Console.Clear();
            string[] usersFirstName = new string[5];
            string[] usersLastName = new string[5];

            
            Console.WriteLine("Please enter user 1's first name: ");
            usersFirstName[0] = Console.ReadLine();
            Console.WriteLine("Please enter user 1's last name: ");
            usersLastName[0] = Console.ReadLine();
            Console.WriteLine("Please enter user 2's first name: ");
            usersFirstName[1] = Console.ReadLine();
            Console.WriteLine("Please enter user 2's last name: ");
            usersLastName[1] = Console.ReadLine();
            Console.WriteLine("Please enter user 3's first name: ");
            usersFirstName[2] = Console.ReadLine();
            Console.WriteLine("Please enter user 3's last name: ");
            usersLastName[2] = Console.ReadLine();
            Console.WriteLine("Please enter user 4's first name: ");
            usersFirstName[3] = Console.ReadLine();
            Console.WriteLine("Please enter user 4's last name: ");
            usersLastName[3] = Console.ReadLine();
            Console.WriteLine("Please enter user 5's first name: ");
            usersFirstName[4] = Console.ReadLine();
            Console.WriteLine("Please enter user 5's last name: ");
            usersLastName[4] = Console.ReadLine();
           
          
            for (int i = 0; i < usersFirstName.Length && i < usersLastName.Length; ++i)
            {
                Console.WriteLine($"Please enter user {i + 1}'s first name: ");
                usersFirstName[i] = Console.ReadLine();
                Console.WriteLine($"Please enter user {i + 1}'s last name: ");
                usersLastName[i] = Console.ReadLine();
            }


            Console.WriteLine("\n---- List of Users ----");
            for (int userIndex = 0; userIndex < usersFirstName.Length; ++userIndex)
            {
                Console.WriteLine($"{userIndex + 1}: {usersFirstName[userIndex]} {usersLastName[userIndex]}");
            }

            

            // methods and functions
            //
            //
            // create reusable sections of code
            // methods evaluate and return a value
            // procedures don't return a value
            
            
            // calling the procedure
            DisplayGreeting("Hello World");

            int userSelectedInt = GetInt();
            Console.WriteLine($"The user entered the value {userSelectedInt}");


            // Pause before closing
            Console.WriteLine("Press a key to continue");
            Console.ReadKey();
        }
        // Declare a procedure
        // return type: void
        // method name: DIsplayGreeting
        // method parameters: (string message)
        
        static void DisplayGreeting(string message)
        {
            Console.WriteLine(message);
        }

        //create a method to validate an integer input
        //validation - getInt
        static int GetInt()
        {
            int validatedInt = 0;
            string input = null;

            do
            {
                Console.Write("Please enter an integer value: ");
                input = Console.ReadLine();
            } while (Int32.TryParse(input, out validatedInt) == false);

            return validatedInt;

        }
    }
}
