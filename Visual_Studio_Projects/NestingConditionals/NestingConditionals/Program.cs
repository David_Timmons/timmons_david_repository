﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NestingConditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            //Nesting Conditionals
            //Some decisions affect other decisions

            //if it is warm enough, lets go to the beach
            //if not lets see a movie

            //if the water is warm, lets go swimming
            //if not, lets get a tan

            int waterTemp = 78;
            int temp = 90;
            if (temp >= 85)
            {
                Console.WriteLine("It is warm enough, lets go to the beach.");
                if (waterTemp >= 75)
                {
                    Console.WriteLine("The Water is warm enough to go swimming.");
                }
                else
                {
                    Console.WriteLine("The water is too cold, lets get a tan.");
                }

            }
            else
            {
                Console.WriteLine("It is too cold for the beach, lets go to the movies.");
            }
           
            
        }
    }
}
