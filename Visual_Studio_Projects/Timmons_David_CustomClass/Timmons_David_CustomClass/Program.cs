﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timmons_David_CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {
            /*David Timmons
             * SDI Section 03
             * 6/22/17
             * Custom Class
             
             */
            //declaring and defining a new hero
            Hero Mario = new Hero(1000, 0, 1000, 0);
            //instantiating the custom class and all that it does.
            Mario.Interact(Mario.GetMax(), 0, 0);

            //I tried to get this to Mario.SetMax from Console.ReadLine();
            //Needless to say, I couldn't get it figured out in time
            //Everything else from the rubric should be present and functional here though
            //Thanks for all your help over the last month!
        }
    }
}
