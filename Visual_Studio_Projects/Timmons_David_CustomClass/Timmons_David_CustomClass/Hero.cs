﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timmons_David_CustomClass
{
    class Hero
    {
        //Create the member variables for the custom class
        decimal mMaxHealth;
        decimal mMinHealth;
        decimal mCurrentHealth;
        decimal mDamage;

        //Create a constructor function using the member variables as parameters
        public Hero (decimal _maxHealth, decimal _minHealth, decimal _currentHealth, decimal _damage)
        {
            mMaxHealth = _maxHealth;
            mMinHealth = _minHealth;
            mCurrentHealth = _currentHealth;
            mDamage = _damage;
        }
        //Create getters for each member variable
        public decimal GetMax()
        {
            return mMaxHealth;
        }
        public decimal GetMin()
        {
            return mMinHealth;
        }
        public decimal GetCurrent()
        {
            return mCurrentHealth;
        }
        public decimal GetDamage()
        {
            return mDamage;
        }
        //Create Setters for each member variable
        public void SetMax(decimal _maxHealth)
        {
            this.mMaxHealth = _maxHealth;
        }
        public void SetMin(decimal _minHealth)
        {
            this.mMinHealth = _minHealth;
        }
        public void SetCurrent(decimal _currentHealth)
        {
            this.mCurrentHealth = _currentHealth;
        }
        public void SetDamage(decimal _damage)
        {
            this.mDamage = _damage;
        }
        //Create custom method to change the value of current health
        public decimal Interact(decimal _currentHealth, decimal _minHealth, decimal _damage)
        {
            //assigning member variables to user input
            this.mCurrentHealth = _currentHealth;
            this.mMinHealth = _minHealth;
            this.mDamage = _damage;
            //Creating the loop to cycle through and change the value of mCurrentHealth
            //Conditional to tell the machine when to break the while loop
            while ((!(mCurrentHealth < mMinHealth)) && (!(mCurrentHealth > mMaxHealth)))
            {
                
                Console.WriteLine("The heros current health is {0}. Is the heros health going to go up or down?", mMaxHealth);
                string upOrDown = Console.ReadLine();
                //validating that the user input was in the correct form and format
                if ((upOrDown == "up") || (upOrDown == "Up"))
                {
                    Console.WriteLine("By how much?");
                    string damageString = Console.ReadLine();
                    while (!decimal.TryParse(damageString, out mDamage))
                    {
                        Console.WriteLine("Please enter a number value.");
                        damageString = Console.ReadLine();
                    }
                    //changing the values of the member variables according to user input
                    mCurrentHealth = mCurrentHealth + mDamage;
                }
                //same deal as before, validation for form and format
                else if ((upOrDown == "down") || (upOrDown == "Down"))
                {
                    Console.WriteLine("By how much?");
                    string damageString = Console.ReadLine();
                    while (!decimal.TryParse(damageString, out mDamage))
                    {
                        Console.WriteLine("Please enter a number value.");
                        damageString = Console.ReadLine();
                    }
                    //changing the values of the member variables according to user input
                    mCurrentHealth = mCurrentHealth - mDamage;
                }
                else
                {
                    //reprompting if user doesn't know how to spell up or down
                    Console.WriteLine("You did not enter either up or down. Please enter either up or down.");
                    upOrDown = Console.ReadLine();
                }

            }
            //when the while loop breaks, letting the user know what happened.
            Console.WriteLine("The heros health went above the maximum or below the minimum.");
            //returning the final value of mCurrentHealth to the name of the custom method
            return mCurrentHealth;

        }













    }
}
