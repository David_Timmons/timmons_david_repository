﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindErrorsFunctions
{
    class Program
    {
        static void Main(string[] args)
        {
            //David Timmons
            //6/14/17
            //SDI Section 03
            //Find The Errors In Functions

            //In this program we will be asking for 2 prices for the user
            //We will ask for the sales tax rate
            //Create a function that will return the price + sales tax
            //Create a function that will add the 2 prices with sales tax together for the total cost

            Console.WriteLine("Hello and welcome to our purchase calculator!\r\nWe will be asking you for 2 item prices and the sales tax rate.\r\n");
            //Gather first user input
            Console.WriteLine("What is the cost of your first item?");
            string cost1String = Console.ReadLine();
            //Declare a new variable to hold the converted value
            decimal cost1;
            while (!decimal.TryParse(cost1String, out cost1))
            {
                Console.WriteLine("Please only type in numbers!\r\nWhat is the cost of your first item?");
                cost1String = Console.ReadLine();
            }
            //Gather second user input
            Console.WriteLine("What is the cost of your second item?");
            string cost2String = Console.ReadLine();
            //Declare a new variable to hold the converted value
            decimal cost2;
            while (!decimal.TryParse(cost2String, out cost2))
            {
                Console.WriteLine("Please only type in numbers!\r\nWhat is the cost of your second item?");
                cost2String = Console.ReadLine();
            }
            //Gather third user input
            Console.WriteLine("What is the sales tax rate %?");  
            string salestaxString = Console.ReadLine();
            //Declare a new variable to hold the converted value
            decimal salesTax;
            while (!decimal.TryParse(salestaxString, out salesTax))
            {
                Console.WriteLine("Please only type in numbers!\r\nWhat is the sales tax rate in %?");
                salestaxString = Console.ReadLine();
            }
            Console.WriteLine("I have all the information I need.\r\nYour first item costs {0}.\r\nYour second item costs {1} and the sales tax is {2}%.", cost1, cost2, salesTax);

            decimal cost1WithTax = AddSalesTax1(cost1, salesTax);
            decimal cost2WithTax = AddSalesTax2(cost2, salesTax);
            decimal grandTotal = TotalCosts(cost1WithTax, cost2WithTax);

            Console.WriteLine("\r\nWith tax your first item costs {0}.\r\nYour second item costs {1}.", cost1WithTax, cost2WithTax);
            Console.WriteLine("\r\nWhich makes the total for your bill {0}", grandTotal);
            //Close of the main method
        }
        //Make a method to find the first price with tax and return the data
        public static decimal AddSalesTax1(decimal firstPrice, decimal tax)
        {
            decimal totalWithTax1 = firstPrice + firstPrice * (tax / 100);
            return totalWithTax1;
        }
        //Make a method to find the second price with tax and return the data
        public static decimal AddSalesTax2(decimal secondPrice, decimal tax)
        {
            decimal totalWithTax2 = secondPrice + secondPrice * (tax / 100);
            return totalWithTax2;
        }
        //Make a method to find the total of both prices and return the data
        public static decimal TotalCosts(decimal firstPriceWithTax, decimal secondPriceWithTax)
        {
            decimal total = firstPriceWithTax + secondPriceWithTax;
            return total;
        }

        /* Test Values
         * Entering an initial value of 10, a second value of 20, and a third value of 8 provides the results, $10.80, $21.60, and $32.40
         * Entering an initial value of $35.50, a second value of $25.15 and a third value of 7.5 provides the results, $38.16, $27.04, and $65.20
         * Entering an initial value of 100, and second value of 100, and a third value of 10 provides the results, $110.00, $110.00, and $220.00
         */

    }
}
