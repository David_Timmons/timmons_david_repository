﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timmons_David_Madlibs
{
    class Program
    {
        static void Main(string[] args)
        {
            /*David Timmons
             * SDI Section 03
             * Timmons_David_Madlibs
             * 06/01/17
             * 
             * My MadLib Generator is in the form of a (really) bad joke.
             * I hope you find it as amusing as I did writing it.
             * 
             */
             //Greeting
            Console.WriteLine("Welcome To The David Timmons Bad Joke Generator! \r\nYou Will Now Be Prompted To Provide Several Random Answers To Several Random Questions!");

            //Creating An Engaging Atmosphere
            Console.WriteLine("Please Press Enter To Be A Part Of Something Stupid.");
            string somethingStupid = Console.ReadLine();

            //Gathering Information About The User
            Console.WriteLine("First Off, What Is Your Name?");
            string userName = Console.ReadLine();
            Console.WriteLine("Do You Prefer Himself or Herself?");
            string gender = Console.ReadLine();

            //Gathering Our User Promted Variables And Assigning Them Names 
            Console.WriteLine("Now, Please Type The Name Of A Thing That Is Wet");
            string wetThing = Console.ReadLine();
            Console.WriteLine("Next, Please Type The Name Of An Occupation With No Pay");
            string priestThing = Console.ReadLine();
            Console.WriteLine("Here, Type The Name Of A Potential Murder Weapon");
            string killerThing = Console.ReadLine();
            Console.WriteLine("Finally, Please Type The Name Of A Place You Wouldn't Want To Run Into Your Father-In-Law");
            string stripClubThing = Console.ReadLine();

            //Furthering The Atmosphere That We've Already Created
            Console.WriteLine("Thank You For Your Help "+userName+". I Would Love To Tell You All This Effort Will Be Worth It.....\r\nBut....\r\n....I Don't Make Promises I Can't Keep.");
            Console.WriteLine("Just A Few More Questions.");
            Console.WriteLine("Please Press Enter If You Are Still Interested In Being A Part Of Something Stupid.");
            string stillStupid = Console.ReadLine();

            //Gathering Our User Promted Number Variables As Strings And Assigning Them Names
            Console.WriteLine("Please Type The Number Of Zits Currently On Your Entire Body");
            string zitsString = Console.ReadLine();
            Console.WriteLine("Please Type The Number Of People You Know....\r\n.....That You Wish Were Stubbing Their Toes Right Now");
            string peopleHatedString = Console.ReadLine();
            Console.WriteLine("And Finally, Please Type A Number A Teenager Would Chuckle At");
            string sixtyNineOrFourTwentyString = Console.ReadLine();

            //Converting Our User Prompted Number Variables From Strings To Usable Number Datatypes
            decimal zitNumber = decimal.Parse(zitsString);
            decimal peopleHatedNumber = decimal.Parse(peopleHatedString);
            decimal sixtyNineOrFourTwentyNumber = decimal.Parse(sixtyNineOrFourTwentyString);

            //A Bit More Engagement And Atmosphere
            Console.WriteLine("Thank You For Your Cooperation, "+userName);
            Console.WriteLine("Please Press Enter One More Time If You Are Still Committed To The Stupid Thing That Happens Next.");
            string evenStillStupid = Console.ReadLine();

            //Being Funny (Hopefully)
            Console.WriteLine("Okay, But You Only Have Yourself To Blame.");
            Console.WriteLine("Press Enter To Deeply Regret Your Willingness To Participate In This Activity");
            string regretThis = Console.ReadLine();

            //Concatenated Story Output
            //All User Prompted Values Are Present
            Console.WriteLine("So A " + priestThing+" named "+userName+ ", A " + wetThing + " and a " + killerThing + " walk into a " + stripClubThing);
            Console.WriteLine("The "+wetThing+ " bets the others "+sixtyNineOrFourTwentyNumber+ " dollars they can't do "+zitNumber+" pushups.");
            Console.WriteLine("If they can, the " + wetThing + " will give them both " + peopleHatedNumber + " wishes.");
            Console.WriteLine(""+userName+ " doubts " +gender+ " but decides to go along with the bet anyways.");
            Console.WriteLine(""+userName+ " and the "+killerThing+ " both do all of their pushups and demand their rewards from the "+wetThing+".");
            Console.WriteLine("The "+wetThing+ " looks them both dead in the eyes and says,");
            Console.WriteLine("Look, Theres no wishes. I'm a "+wetThing+ " and the dude who's writing this is an amateur programmer.");
            Console.WriteLine("What did you think, I'd be a "+wetThing+ " genie and David would be a comedian?");

            //Turns Out I'm No Comedian After All
            Console.WriteLine("Please press enter to acknowledge that you were warned this would be stupid.");
            string toldYouSo = Console.ReadLine();

            //Sign Off
            Console.WriteLine("Thank you for using the David Timmons Bad Joke Generator! \r\nCome back when I'm a better programmer, but don't hold your breath for better jokes!");

            






        }
    }
}
