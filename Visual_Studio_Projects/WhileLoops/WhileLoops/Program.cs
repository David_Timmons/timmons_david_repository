﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            //declare and define a starting counting variable

            int counter = 10;

            while (counter > 0)
            {
                Console.WriteLine("The counter is {0}.", counter );
                //add increment of change to counter variable
                counter--;}

            Console.WriteLine("Please enter your name.");
            string userName = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(userName))
            {
                Console.WriteLine("Please do not leave this area blank. \r\nPlease enter your name");
                userName = Console.ReadLine();

            }


            //Convert to number and validate
            Console.WriteLine("Hi {0}! Please enter a number:" ,userName);
            string numberString = Console.ReadLine();

            //declare the variable to hold the converted value
            double userNum;

            //Whille loop validation process
            while (!double.TryParse(numberString, out userNum))
            {
                Console.WriteLine("You have typed something other than a number.\r\nPlease enter a number.\r\n");
                numberString = Console.ReadLine();
            }

            Console.WriteLine("\r\nThe number you entered is {0}" ,userNum);

            //Declare and define the variable to test
            int i = 20;

            //Do while loops run at least once and then they check
            do {
                Console.WriteLine("The value of i is {0}.", i);
                i--;
            } while (i>=0);
                   

        }

    }
}
