﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDI_Find_Errors_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            //  David Timmons
            //  06/07/17
            //  SDI Section 03 
            //  Find The Errors Conditionals
            //  Find and fix the errors

            String myName = "John Doe";
            String myJob = "Cat Wrangler";

            //Changed myRatePerCat from double to decimal so that I am able to combine them later on.
            //Made sure to add the suffix m so the machine knows we're dealing with decimals.
            decimal myRatePerCat = 7.50m;
            decimal totalPay = 0.00m;

            //Changed "boolean" to keyword "bool."
            int numberOfCats = 40;
            bool employed = true;

            //Fixed all the grammatical errors in this chunk of code.
            Console.WriteLine("Hello!  My name is " + myName + ".");
            Console.WriteLine("I'm a " + myJob + ".");
            Console.WriteLine("My current assignment has me wrangling " + numberOfCats + " cats.");
            Console.WriteLine("So, let's get to work!");

            //Changed the value of numberOfCats from greater than 0 to greater than 5.
            //This will stop the while loop at the count of 5 cats, which as we know is where our trusty cat wrangler faces imminent unemployment.
            while (numberOfCats > 5)
            {

                if (employed = true)
                {
                    //To gather the total amount of money earned, we make totalPay = totalPay + myRatePerCat.
                    //This will change the value of totalPay at each iteration of the loop.
                    //Fixed all the grammatical errors in this chunk of code.
                    totalPay = totalPay + myRatePerCat;
                    Console.WriteLine("I've wrangled another cat and I have made $" + totalPay + " so far.  \r\nOnly " + numberOfCats + " left!");

                }

                //Decrement operation on numberOfCats is outside of the first conditional,
                //But still inside the while loop, so after each iteration of the conditional,
                //The decrement removes one from the total of numberOfCats.
                numberOfCats--;

                //Moved the "ive been fired" chunk of code to the second conditional statement.
                //Now, the program asks if the number of cats is equal to 5, once the first conditional has finished.
                //Because the number of cats is five, the second conditional triggers,
                //Leaving our trusty cat wrangler out of work. 

                if (numberOfCats == 5)
                {

                    employed = false;
                    Console.WriteLine("I've been fired!  Someone else will have to wrangle the rest!");
                    continue;

                }

            }

        }
    }
}





