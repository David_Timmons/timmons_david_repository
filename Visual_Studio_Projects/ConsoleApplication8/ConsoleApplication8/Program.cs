﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication8
{
    class Program
    {
        


public static void Main(string[] args)
        {
     int lastScore;
     int[] examScores = new int[] { 100, 92, 90, 100, 88 };
     lastScore = 70;
     AddScore(lastScore, examScores);
}
public static void AddScore(int _grade, int[] _gradeArray)
        {
     int totalOfScores = 0;
     int numExams = _gradeArray.Length;
     double finalGrade;
     for (int i = 0; i < numExams; i++)
            {
          totalOfScores = totalOfScores + _gradeArray[i];
     }
     totalOfScores += _grade;
     finalGrade = totalOfScores / (_gradeArray.Length + 1);
     finalGrade = Math.Round(finalGrade);
     Console.WriteLine("Your average for the exams is { 0}.", finalGrade + ".");

}
    }
    }

