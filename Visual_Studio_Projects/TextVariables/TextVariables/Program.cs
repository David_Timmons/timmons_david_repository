﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextVariables
{
    class Program
    {
        /* David Timmons
         * SDI Section 03
         * Variables Character Type
         * 05/31/17
         */
        static void Main(string[] args)
        {
            //Variables - Characters
            //Declare a variable

            //Characters - char - only the size of one character
            //Must be surrounded by single quotes

            char firstLetter;

            //Define a variable
            firstLetter = 'A';

            //Print the variable to the console

            Console.WriteLine(firstLetter);

            //Declare and define a variable

            char secondCharacter = 'B';
            //Print this new vairable to the console

            Console.WriteLine(secondCharacter);

            //String - Anything bigger than a single character 
            //Must be surrounded by double quotes

            string wholeSentence = ("This is an example of a sentence");

            //Print out the variable

            Console.WriteLine(wholeSentence);

            //Combine Strings
            //Using the + sign - Concatenation
            string combinedString = "firstPart "+"secondPart";
            Console.WriteLine(combinedString);

            //Spaces?
            string firstName = "Kermit";
            string lastName = "The Frog";
            string wholeName = firstName + " "+ lastName;

            Console.WriteLine(wholeName);

          



        }
    }
}
