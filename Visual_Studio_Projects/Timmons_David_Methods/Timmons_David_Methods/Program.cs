﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timmons_David_Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            //Prompt and validate
            Console.WriteLine("Welcome to problem 1, Painting A Wall!");

            Console.WriteLine("What is the width of the wall?");
            string widthString = Console.ReadLine();

            decimal width;
            while (!(decimal.TryParse(widthString, out width)))
            {
                Console.WriteLine("Please enter a number value");
                widthString = Console.ReadLine();
            }

            //Prompt and validate

            Console.WriteLine("What is the height of the wall?");
            string heightString = Console.ReadLine();

            decimal height;
            while (!(decimal.TryParse(heightString, out height)))
            {
                Console.WriteLine("Please enter a number value");
                heightString = Console.ReadLine();
            }

            //Prompt and validate

            Console.WriteLine("How many coats of paint?");
            string paintString = Console.ReadLine();

            decimal paint;
            while (!(decimal.TryParse(paintString, out paint)))
            {
                Console.WriteLine("Please enter a number value");
                paintString = Console.ReadLine();
            }

            //Prompt and validate

            Console.WriteLine("How many square feet will one gallon of paint cover?");
            string feetString = Console.ReadLine();

            decimal feet;
            while (!(decimal.TryParse(feetString, out feet)))
            {
                Console.WriteLine("Please enter a number value");
                feetString = Console.ReadLine();
            }

            //Declaring variables, Calling functions, providing output
            decimal wallSpecs = CalcArea(width, height);
            decimal paintNeeded = CalcFeetSquared(wallSpecs, paint, feet);
            Console.WriteLine("The area of the wall is {0}", wallSpecs);
            Console.WriteLine("You need {0} gallons of paint for {1} coats of paint on that wall.", paintNeeded, paint);

            Console.WriteLine("\r\n");

            //Labeling the section and prompting for user input
            Console.WriteLine("Welcome to problem 2, Stung!\r\nHow much do you weigh?");
            string weightString = Console.ReadLine();
            //Validating
            decimal weight;
            while (!(decimal.TryParse(weightString, out weight)))
            {
                Console.WriteLine("Please enter a number value.");
                weightString = Console.ReadLine();
            }
                decimal toKillYouDead = ToKillYou(weight);

                Console.WriteLine("With a weight of {0} it will take {1} beestings to kill you dead.", weight, toKillYouDead);

            Console.WriteLine("\r\n");

            //I built this whole thing around my own custom array, and did the test results after, 
            //My test array is the first one in the test values at the bottom of the section
            //Labeling the section and creating the initial array
            Console.WriteLine("Welcome To Problem 3, Reverse It!");
            string[] myArray = new string[7] { "Red", "Orange", "Yellow", "Green", "Blue", "Indigo", "Violet" };

            
            //creating the loop to iterate through the replacement array
            foreach (string item in ReturnArray(myArray))
            {
                Console.WriteLine(item);
            }
        }
        //Creating the functions to do the heavy lifting and get called for problem 1
        public static decimal CalcArea(decimal w, decimal h)
        {
            decimal area = w * h;
            return area;
        }
        public static decimal CalcFeetSquared(decimal wallSpecs, decimal coats, decimal cover)
        {
            decimal feetCovered = wallSpecs * coats / cover;
            return feetCovered;
        }

        /* Test Values
         * Entering the values 22.2, 30, 1, and 1 provides the result "You need 666 gallons of paint for 1 coat of paint on that wall."
         * Entering the values 8, 10, 2, and 200 provides the result "You need 0.5333333333333333333333333333 gallons of paint for 2 coats of paint on that wall."
         * Entering the values 30, 12.5, 3, 350 provides the result "You need 3.2142857142857142857142857143 gallons of paint for 3 coats of paint on that wall."
         */

        //Creating the functions to do the heavy lifting and get called for problem 2
        public static decimal ToKillYou(decimal weight)
        {
            decimal dead = weight * 9;
            return dead;
        }

        /* Test Values
         * Entering the value 10 provides the result "It will take 90 beestings to kill you dead."
         * Entering the value 160 provides the result "It will take 1440 beestings to kill you dead."
         * Entering the value 74 provides the result "IT will take 666 beestings to kill you dead."
         * Entering the value "twenty" prompts the user to enter a valid number value.
         */

        public static string[] ReturnArray(string[] myArray)
        {
            //Creating the array to hold the reversed value
            string[] sendArray = new string[7] {"","","","","","",""};
            //Creating the variable to translate the array values
            int k = ((sendArray.Length - 1) - (sendArray.Length - 1));
            //The loop to replace i with k in the empty array
            for (int i = myArray.Length - 1; i >= 0; i--)
            {
                sendArray[k] = myArray[i];
                k++;
            }
            //returning the reversed array to the function
            return sendArray;
         }
        /* Test Values
         * Creating a 10 string array with the elements "I", "Will","Find","A","Way","To","Make","This","Equal","Six-Hundred And Sixty-Six" provides the result:
         * Six-Hundred And Sixty-Six
           Equal
           This
           Make
           To
           Way
           A
           Find
           Will
           I
           Creating a five string array with the elements "Apple", "Pear", "Peach", "Coconut", "Kiwi" provides the result:
           Kiwi
           Coconut
           Peach
           Pear
           Apple
           Creating a seven string array with the elements "Red", "Orange", "Yellow", "Green", "Blue", "Indigo", "Violet" provides the result:
           Violet
           Indigo
           Blue
           Green
           Yellow
           Orange
           Red
         */



















    }
}
