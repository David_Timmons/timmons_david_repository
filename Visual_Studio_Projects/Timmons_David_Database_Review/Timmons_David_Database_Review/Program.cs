﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Timmons_David_Database_Review
{
    class Program
    {
        static void Main(string[] args)
        {
            bool running = true;
            while (running)
            {
                
                Console.WriteLine("\r\nPlease select a fucntion: \r\n1. Select a city\r\n2. Exit\r\n--------------------------");
                string menuInput = Console.ReadLine().ToLower();

                switch (menuInput)
                {
                    case "1":
                    case "select a city":
                        {
                            string cs = @"server=192.168.8.1;userid=dbsAdmin;password=password;database=World; port=8889";

                            MySqlConnection conn = null;

                            try
                            {
                                conn = new MySqlConnection(cs);
                                conn.Open();

                               /* string deleteOrlandoFromCity = "DELETE from city where city.name = 'Orlando'";
                                MySqlCommand deleteCmd = new MySqlCommand(deleteOrlandoFromCity, conn);
                                string cityDeleteOutput = Convert.ToString(deleteCmd.ExecuteScalar());
                                string deleteOrlandoFromWeather = "DELETE from weather where weather.city = 'Orlando'";
                                MySqlCommand deleteCmd1 = new MySqlCommand(deleteOrlandoFromWeather, conn);
                                string weatherDeleteOutput = Convert.ToString(deleteCmd1.ExecuteScalar());
                                string addOrlandoToCity = "INSERT INTO city(Name, CountryCode, District) values ('Orlando', 'USA', 'Florida')";
                                MySqlCommand addCmd = new MySqlCommand(addOrlandoToCity, conn);
                                string cityAddOutput = Convert.ToString(addCmd.ExecuteScalar());
                                string addOrlandoToWeather = "INSERT INTO weather(city, temp, pressure, humidity) values ('Orlando', '57', '100', '64')";
                                MySqlCommand addCmd1 = new MySqlCommand(addOrlandoToWeather, conn);
                                string weatherAddOutput = Convert.ToString(addCmd1.ExecuteScalar());
                               */

                                Console.WriteLine("Please enter the name of a city. ");
                                string userInput = Console.ReadLine();

                                string temp = $"SELECT temp from weather join city on city.Name = weather.City where weather.City = '{userInput}'";
                                string pressure = $"SELECT pressure from weather join city on city.Name = weather.City where weather.City = '{userInput}'";
                                string humidity = $"SELECT humidity from weather join city on city.Name = weather.City where weather.City = '{userInput}'";

                                MySqlCommand tempCmd = new MySqlCommand(temp, conn);
                                MySqlCommand pressureCmd = new MySqlCommand(pressure, conn);
                                MySqlCommand humidityCmd = new MySqlCommand(humidity, conn);
                                string tempOutput = Convert.ToString(tempCmd.ExecuteScalar());
                                string prsOutput = Convert.ToString(pressureCmd.ExecuteScalar());
                                string hmuOutput = Convert.ToString(humidityCmd.ExecuteScalar());

                                if (tempOutput != "")
                                {
                                    Console.WriteLine("City Name: {0}\r\nTemp : {1}\r\nPressure : {2}\r\nHumidity : {3}", userInput, tempOutput, prsOutput, hmuOutput);
                                }
                                else
                                {
                                    Console.WriteLine("There is no city by that name. ");
                                }

                            }
                            catch (MySqlException ex)
                            {
                                Console.WriteLine("Error: {0}", ex.ToString());

                            }
                            finally
                            {

                                if (conn != null)
                                {
                                    conn.Close();
                                }

                            }

                            Console.WriteLine("Done");

                        }
                        break;
                    case "2":
                    case "exit":
                        {
                            Console.WriteLine("Goodbye. ");
                            running = false;
                        }
                        break;
                }


            }
        }
    }
}
