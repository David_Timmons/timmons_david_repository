﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserPrompts
{
    class Program
    {
        static void Main(string[] args)
        {
            //Read Line Method of the Console
            //A method is something the console can do
            //Reads the next line in the console and returns the text string back to your code

            //Only returns a string
            //Ask the user their name


            //Ask the question first then listen for the response

            Console.WriteLine("Please Type In Your Name And Press Return");

            //Listen for the answer
            //Create a variable to catch the returned text string
            string userName = Console.ReadLine();

            //Say hello to the user

            Console.WriteLine("Hello "+userName+", Welcome To Rectangle Perimeter Finder Thing!");


            //Ask them for numeric values

            //Caluculate the perimiter of a rectangle
            // width*2 + length*2
            //Ask the user for width and length

            //Tell the user what we're doing

            Console.WriteLine("Let's Find Out The Perimiter Of A Rectangle!\r\nPlease Enter A Width And Press Return");
            //listen for the answer, which is a string, then convert
            string widthString = Console.ReadLine();

            //Convert the string to a number datatype

            double widthNumber = double.Parse(widthString);

            Console.WriteLine("Now Please Enter A Length");

            //Store the answer as a string

            string lengthString = Console.ReadLine();

            //Convert string to int

            double lengthNumber = double.Parse(lengthString);

            Console.WriteLine(lengthNumber);

            //user gets the perimiter of a rectangle

            double perimeterRect = widthNumber * 2 + lengthNumber * 2;
            Console.WriteLine(""+userName+", The Perimeter Of A Rectangle With Your Chosen Dimensions Is: "+perimeterRect);
            Console.WriteLine("Thank You For Using Rectangle Perimeter Finder Thing!");

            //If user types in decimal place, we have issues
            //Change int to double
         




        }
    }
}
