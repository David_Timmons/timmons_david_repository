﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SteakTemperature
{
    class Program
    {
        static void Main(string[] args)
        {
            /*David Timmons
             06/06/17
             Steak Temperature
             Section 03*/

            //Determine what cook temp our steak is

            /* Rare - 130 - 140
             * Medium Rare - 140 - 145
             * Medium - 145 - 160
             * Well Done - 160 - 170
             */

            //Ask the user for temperature of their steak

            Console.WriteLine("We are going to determine the steak doneness level.\r\nWhat is the temperature of your steak in degrees F?\r\n Please press return when done.");
            string steakTempString = Console.ReadLine();

            //Convert to a number

            int steakTemp = int.Parse(steakTempString);

            //start out conditional

            //test the temp against doneness level



            if (steakTemp < 130)
            {
                Console.WriteLine("This steak is undercooked and not fit for consumption.");
            }
            else if (steakTemp < 140)
            {
                Console.WriteLine("This steak is rare.");
            }
            else if (steakTemp < 145)
            {
                Console.WriteLine("This steak is medium rare.");
            } else if (steakTemp < 160)
            {
                Console.WriteLine("This steak is medium.");
            } else if (steakTemp < 170)
            {
                Console.WriteLine("This steak is well done.");
            } else 
                   Console.WriteLine("This is fucking shite madam!");
                
           
        }
    }
}
