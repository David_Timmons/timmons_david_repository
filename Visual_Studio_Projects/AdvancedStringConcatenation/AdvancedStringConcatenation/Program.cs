﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedStringConcatenation
{
    class Program
    {
        static void Main(string[] args)
        {
            double myNum = 12.345;
            Console.WriteLine("The value is " +myNum);

            //use a placeholder or parameter
            //index starts at zero

            string test = String.Format("The meaning of life is {0}" , 42);
            Console.WriteLine(test);

            double otherNum = 56.7;
            Console.WriteLine("The first number is {0} and the second number is {1}.", myNum, otherNum);

            int i = 0;
            while (i < 10)
            {
                Console.WriteLine(i);
                i++;
            }


        }
    }
}
