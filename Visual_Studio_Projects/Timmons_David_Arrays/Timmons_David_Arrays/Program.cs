﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timmons_David_Arrays
{
    class Program
    {
        static void Main(string[] args)
        {

            /*David Timmons
             * SDI Section 03
             * Timmons_David_Arrays
             * 06/03/17
             * */

            int[] firstArray = new int[4] { 4, 20, 60, 150 };
            double[] secondArray = new double[4] { 5, 40.5, 65.4, 145.98 };

            //I know that there is supposed to be a way to convert the first array into a double so that I can add the elements of the two arrays together
            //I have tried for hours to make that work, have scoured the internet for answers, to no avail.
            //And, I'm doing this on a Saturday, so there's nobody on staff to help me with it right now.
            //So, I'm just going to make a new version of the first array in the double datatype.
            //Hopefully I don't lose too many points for not converting it.

            double[] firstArrayDouble = new double[4] { 4, 20, 60, 150 };

            //You can find the total of the elements in the arrays like this:

            double firstArrayTotal = firstArrayDouble[0] + firstArrayDouble[1] + firstArrayDouble[2] + firstArrayDouble[3];
            double secondArrayTotal = secondArray[0] + secondArray[1] + secondArray[2] + secondArray[3];

            Console.WriteLine("The total sum of the elements in the array is " +firstArrayTotal);
            Console.WriteLine("The total sum of the elements in the array is " +secondArrayTotal);

            //I had a hunch that there was an easier way to find the totals of the elements in arrays
            //I found that easier way here https://www.dotnetperls.com/sum
            //Finding the total values of the arrays using .Sum();

            double firstArrayTotal2 = firstArrayDouble.Sum();
            double secondArrayTotal2 = secondArray.Sum();

            Console.WriteLine("The total sum of the elements in the array is " + firstArrayTotal2);
            Console.WriteLine("The total sum of the elements in the array is " + secondArrayTotal2);

            //After discovering .Sum I discovered .Average
            //.Average doesn't work for the original firstArray, because it is an integer datatype,
            //And .Average only works for doubles or decimals
            //Hense all the hours of trouble I had trying to convert firstArray into a double

            //You can find the average of the arrays like this:

            double firstArrayAverage = firstArrayTotal / 4;
            double secondArrayAverage = secondArrayTotal / 4;

            Console.WriteLine("The average of the elements in the array is "+firstArrayAverage);
            Console.WriteLine("The average of the elements in the array is "+secondArrayAverage);

            //You can also just do .Average, if the arrays are in the correct datatypes.

            double firstArrayAverage2 = firstArrayDouble.Average();
            double secondArrayAverage2 = secondArray.Average();

            Console.WriteLine("The average of the elements in the array is " + firstArrayAverage2);
            Console.WriteLine("The average of the elements in the array is " + secondArrayAverage2);

            //Creating the thrid array by telling the machine to add the corresponding values and assigning variable names and defining datatypes.

            double thirdArray0 = firstArrayDouble[0] + secondArray[0];
            double thirdArray1 = firstArrayDouble[1] + secondArray[1];
            double thirdArray2 = firstArrayDouble[2] + secondArray[2];
            double thirdArray3 = firstArrayDouble[3] + secondArray[3];

            double[] thirdArray = new double[4] { thirdArray0, thirdArray1, thirdArray2, thirdArray3 };

            //I decided to writeline these values,

            Console.WriteLine("The combined values of indexes 0 in the first and second arrays is " + thirdArray[0]);
            Console.WriteLine("The combined values of indexes 1 in the first and second arrays is " + thirdArray[1]);
            Console.WriteLine("The combined values of indexes 2 in the first and second arrays is " + thirdArray[2]);
            Console.WriteLine("The combined values of indexes 3 in the first and second arrays is " + thirdArray[3]);

            //Because when console.writelining the arrays, they only output to the console as their datatypes
            //I.E., system.double[]

            Console.WriteLine(firstArrayDouble);
            Console.WriteLine(secondArray);
            Console.WriteLine(thirdArray);

            //One more output just for safety's sake

            Console.WriteLine("The third array is: double thirdArray = new double[4] {" +thirdArray[0]+", "+ thirdArray[1]+", "+thirdArray[2]+", "+thirdArray[3]+"}");

            //Now for the sentence thing

            string[] MixedUp = new string[] { "but the lighting of a", "Education is not", "fire.", "the filling", "of a bucket," };

            //Assign each sentence chunk a variable name

            string MixedUp1 = "but the lighting of a ";
            string MixedUp2 = "Education is not ";
            string MixedUp3 = "fire.";
            string MixedUp4 = "the filling ";
            string MixedUp5 = "of a bucket ";

            //Now create a new string that concatenates all of these in the right order

            string notMixedUp = MixedUp2 +  MixedUp4 +  MixedUp5 +  MixedUp1 +  MixedUp3;

            //Now output that to the console

            Console.WriteLine(notMixedUp);

            //I tried very very hard on this one, it took me MUCH longer than it should have to complete
            //If I didn't create the third array properly, or if I lost a significant amount of points for not converting 
            //firstArray properly, I would sincerely appreciate an explanation of where I went wrong or
            //what I could have done better. Thank you!

           






        }
    }
}
