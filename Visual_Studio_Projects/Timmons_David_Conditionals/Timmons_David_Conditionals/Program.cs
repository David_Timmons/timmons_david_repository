﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timmons_David_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {

            /*David Timmons
             * SDI Section 03
             * 06/08/17
             * Conditionals
             */

            //Labeling the section
            Console.WriteLine("Welcome to Problem #1, Temperature Converter!");

            //Prompt the user for the first piece of user input
            Console.WriteLine("Please enter a temperature in integer form, and press return.");
            string tempString = Console.ReadLine();

            //Validate that the user input was not blank, and provide a second chance to get it right
            while (string.IsNullOrWhiteSpace(tempString))
            {
                Console.WriteLine("Please do not leave this field blank. \r\nPlease type in a temperature in integer form and press return.");
                tempString = Console.ReadLine();
            }

            //Vaidate that the user input was not improper format, and provide a second chance to get it right
            //Declare a variable to be on the recieving end of the TryParse
            decimal tempDec;

            //Test whether the user input can be converted to int
            //If it cannot be, display error message
            while (!decimal.TryParse(tempString, out tempDec))
            {
                Console.WriteLine("You entered a value that was not an integer. \r\nPlease enter a temperature in integer form and press return.");
                tempString = Console.ReadLine();
            }

            Console.WriteLine("Thank You.");

            //Gather the second round of user input
            Console.WriteLine("Now Please enter the character F to indicate your temperature is in Fahrenheit, and needs to be converted to Celcius,");
            Console.WriteLine("Or enter the character C to indicate your temperature is in Celcius and needs to be converted to Fahrenheit.");
            string tempType = Console.ReadLine();

            //Validate that the user input was not blank, and provide a second chance to get it right
            while (string.IsNullOrWhiteSpace(tempType))
            {
                Console.WriteLine("Please do not leave this field blank.");
                Console.WriteLine("Now Please enter the character F to indicate your temperature is in Fahrenheit, and needs to be converted to Celcius,");
                Console.WriteLine("Or enter the character C to indicate your temperature is in Celcius and needs to be converted to Fahrenheit.");
                tempType = Console.ReadLine();
            }

            //defining the math for the conversion
            decimal tempF = ((tempDec - 32) / 9) * 5;
            decimal tempC = ((tempDec / 5) * 9) + 32;


            //WHOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO!!!!!!!!!!!!!!!!
            //It only took me like 100 hours but i got it figured out.
            if (!((tempType == "f") || (tempType == "F") || (tempType == "c") || (tempType == "C")))
            {
                Console.WriteLine("You did not enter F or C. Enter F or C.");
                tempType = Console.ReadLine();
            }
           
 
            if (tempType == "f")
            {
                Console.WriteLine("{0} converted from Fahrenheit to Celcius is {1} degrees.", tempString, tempF);
            }
            else if (tempType == "F")
            {
                Console.WriteLine("{0} converted from Fahrenheit to Celcius is {1} degrees.", tempString, tempF);
            }
            else if (tempType == "c")
            {
                Console.WriteLine("{0} converted from Celcius to Fahrenheit is {1} degrees.", tempString, tempC);
            }
            else if (tempType == "C")
            {
                Console.WriteLine("{0} converted from Celcius to Fahrenheit is {1} degrees.", tempString, tempC);
            }
            
            /*
             * Test Values
             * Entering an initial value of 32 and identifying it as F results in 0 Celsius.
             * Entering an initial value of 100 and identifying it as C results in 212 Fahrenheit.
             * Entering an initial value of 50 and identifying it as either c or C results in 122 Fahrenheit.
             * No, the value does not change whether or not the C is upper or lowercase.
             * My test value is 666 degrees Fahrenheit converted to Celsius.
             * 666 degrees Fahrenheit is 352.2222222222222 degrees Celsius. 
             * I confirmed this by using the google temperature converter.
             */

            Console.WriteLine("\r\n\r\n");
            //Problem 2
            //Labeling the section
            Console.WriteLine("Welcome to problem 2, Last Chance For Gas!");
            Console.WriteLine("We're going to find out if you have enough gas to get to a place.");

            //Prompting user input

            Console.WriteLine("How many gallons of gas does your car's gas tank hold?");
            string gasTankString = Console.ReadLine();

            //Validating user input is not blank, and is in correct format
            //Declaring variable to accept converted values

            decimal gasTankDecimal;
            decimal.TryParse(gasTankString, out gasTankDecimal);

            while (string.IsNullOrWhiteSpace(gasTankString))
            {
                Console.WriteLine("Please do not leave this field blank.");
                gasTankString = Console.ReadLine();
            }
            if (!decimal.TryParse(gasTankString, out gasTankDecimal))
            {
                Console.WriteLine("You entered a value that was not a number. \r\nPlease enter a number value and press return.");
                tempString = Console.ReadLine();
            }

            Console.WriteLine("What percentage of your gas tank is currently full? Please enter value in integer form.");
            string gasPercentString = Console.ReadLine();

            //Validating user input is not blank, and is in correct format
            //Declaring variable to accept converted values

            decimal gasPercentDecimal;
            decimal.TryParse(gasPercentString, out gasPercentDecimal);

            while (string.IsNullOrWhiteSpace(gasPercentString))
            {
                Console.WriteLine("Please do not leave this field blank.");
                gasPercentString = Console.ReadLine();
            }
            if (!decimal.TryParse(gasPercentString, out gasPercentDecimal))
            {
                Console.WriteLine("You entered a value that was not a number. \r\nPlease enter a number value and press return.");
                tempString = Console.ReadLine();
            }

            //Converting the users integer input into decimal values
            decimal gasPercentPercent = gasPercentDecimal * 1 / 100;
            

            Console.WriteLine("How many miles does your car get to the gallon?");
            string gasPerGallonString = Console.ReadLine();

            //Validating user input is not blank, and is in correct format
            //Declaring variable to accept converted values
            
            decimal gasPerGallonDecimal;
            decimal.TryParse(gasPerGallonString, out gasPerGallonDecimal);

            while (string.IsNullOrWhiteSpace(gasPerGallonString))
            {
                Console.WriteLine("Please do not leave this field blank.");
                gasPerGallonString = Console.ReadLine();
            }
            if (!decimal.TryParse(gasPerGallonString, out gasPerGallonDecimal))
            {
                Console.WriteLine("You entered a value that was not a number. \r\nPlease enter a number value and press return.");
                tempString = Console.ReadLine();
            }

            //Needs to travel 200 miles
            //To find this out, we need to find how many gallons of gas are in the tank
            //This equation would be gasTankDecimal times gasPercentDecimal

            decimal totalGas = gasTankDecimal * gasPercentPercent;
            Console.WriteLine("Your gas tank currently has {0} gallons of gas in it." ,totalGas);

            //Now we need to find out how many miles this amount of gas will get the user
            //This equation would be totalGas * gasPerGallonDecimal

            decimal totalDistance = totalGas * gasPerGallonDecimal;
            Console.WriteLine("Your car can travel {0} miles with {1} gallons of gas." ,totalDistance, totalGas);

            if (totalDistance > 200)
            {
                Console.WriteLine("Yes, you can travel {0} miles before you need to fill up again!" ,totalDistance);
            }
            else
            {
                Console.WriteLine("No, you only have {0} miles you can drive. Better get gas now while you can!" ,totalDistance);
            }

            /* Test Values
             * Entering values of 20, 50 and 25 produces a result of "Yes, you can travel 250 miles before you need to fill up again."
             * Entering values of 12, 60 and 20 produces a result of "No, you only have 144 miles you can drive. Better get gas now while you can!"
             * Entering Values of 66, 66 and 66 produces a result of "Yes, you can travel 2874.96 miles before you need to fill up again."
             */


            Console.WriteLine("\r\n\r\n");

            //Labeling the section
            //Gathering user input
            
            //Creating an array of possible grade letters to be linked to grade values
            string[] gradeLetters = new string[5] { "F", "D", "C", "B", "A" };

            Console.WriteLine("Welcome To Problem 3, Grade Letter Calculator!");
            Console.WriteLine("Please enter the integer number grade that you recieved for Conditionals.");
            string gradeString = Console.ReadLine();
            
            //Declaring a new decimal to hold converted value
            //Validating that the input is in the right format
            decimal gradeDecimal;
            if (!decimal.TryParse(gradeString, out gradeDecimal))
            {
                Console.WriteLine("You entered a value that was not a number. \r\nPlease enter a number value and press return.");
                gradeString = Console.ReadLine();
            }
            
            //Converting string value to usable number datatype
            //Performing an equation on converted value to change it into percent value
            
            decimal.TryParse(gradeString, out gradeDecimal);
            decimal gradePercent = gradeDecimal * 1 / 100;

            //Validating that the user did not leave the field blank
            while (string.IsNullOrWhiteSpace(gradeString))
            {
                Console.WriteLine("Please do not leave this field blank.");
                gradeString = Console.ReadLine();
            }


            if (gradeDecimal < 69)
            {
                Console.WriteLine("You recieved a grade of {0}% which means you earned a letter grade of " + gradeLetters[0] + " on conditionals.", gradePercent);
            }
            else if (gradeDecimal < 72)
            {
                Console.WriteLine("You recieved a grade of {0}% which means you earned a letter grade of " + gradeLetters[1] + " on conditionals.", gradePercent);
            }
            else if (gradeDecimal < 79)
            {
                Console.WriteLine("You recieved a grade of {0}% which means you earned a letter grade of " + gradeLetters[2] + " on conditionals.", gradePercent);
            }
            else if (gradeDecimal < 89)
            {
                Console.WriteLine("You recieved a grade of {0}% which means you earned a letter grade of " + gradeLetters[3] + " on conditionals.", gradePercent);
            }
            else if (gradeDecimal < 100)
            {
                Console.WriteLine("You recieved a grade of {0}% which means you earned a letter grade of " + gradeLetters[4] + " on conditionals.", gradePercent);
            }
            else if (gradeDecimal > 100)
            {
                Console.WriteLine("You did not enter a valid value. Please enter a valid value.");
                gradeString = Console.ReadLine();
            }

            //I have tried for hours to make the code recognize any input after entering the value 120
            //Nothing works, and no help came to me on slack
            //When you enter a value over 100, it tells you the input is invalid, asks for new input, and does nothing with it
            //No matter what I try 
            //I have nested a new set of if else statements after that,
            //Changed it from else if to just else and nested them,
            //I have no idea what is going wrong from here and cannot fix it.


            /* Test Values
             * Entering an initial input of 92 provides an output of "You recieved a grade of 0.92% which means you earned a letter grade of A on conditionals."
             * Entering an initial input of 80 provides an output of "You recieved a grade of 0.80% which means you earned a letter grade of B on conditionals."
             * Entering an initial input of 67 provides an output of "You recieved a grade of 0.67% which means you earned a letter grade of F on conditionals."
             * Entering an initial input of 120 provides an output of "You did not enter a valid percentage." and prompts the user for more input
             * Entering an initial input of 66.6 provides an output of "You recieved a grade of 0.666% which menas you earned a letter grade of F on conditionals."
             */

            Console.WriteLine("\r\n\r\n");


            Console.WriteLine("Welcome To Problem 4, Discount Double Check!");

            Console.WriteLine("Please enter the cost of your first item.");
            string firstString = Console.ReadLine();

            //Validating user input is in correct format and is not blank
            decimal firstDecimal;
            if (!decimal.TryParse(firstString, out firstDecimal))
            {
                Console.WriteLine("You entered a value that was not a number. \r\nPlease enter a number value and press return.");
                firstString = Console.ReadLine();
            }
            decimal.TryParse(firstString, out firstDecimal);

            while (string.IsNullOrWhiteSpace(firstString))
            {
                Console.WriteLine("Please do not leave this field blank.");
                firstString = Console.ReadLine();
            }



            Console.WriteLine("Please enter the cost of your second item.");
            string secondString = Console.ReadLine();

            //Validating user input is in correct format and is not blank
            decimal secondDecimal;
            if (!decimal.TryParse(secondString, out secondDecimal))
            {
                Console.WriteLine("You entered a value that was not a number. \r\nPlease enter a number value and press return.");
                secondString = Console.ReadLine();
            }
            decimal.TryParse(secondString, out secondDecimal);

            while (string.IsNullOrWhiteSpace(secondString))
            {
                Console.WriteLine("Please do not leave this field blank.");
                secondString = Console.ReadLine();
            }


            decimal totalDecimal = firstDecimal + secondDecimal;

            decimal largeDiscount = totalDecimal * 1 / 10;
            decimal smallDiscount = totalDecimal * 1 / 20;

            string[] discountArray = new string[2] { "10%", "5%" };

            if (totalDecimal >= 100)
            {
                totalDecimal = totalDecimal - largeDiscount;
                Console.WriteLine("The total cost of your purchase, with your {2} discount of ${0} is ${1}.", largeDiscount, totalDecimal, discountArray[0]);
            }
            else if (totalDecimal >= 50)
            {
                totalDecimal = totalDecimal - smallDiscount;
                Console.WriteLine("The total cost of your purchase, with your {2} discount of ${0} is ${1}.", smallDiscount, totalDecimal, discountArray[1]);
            }
            else
            {
                Console.WriteLine("The total cost of your purchase is {0}.",totalDecimal);
            }

            /* Test Values
             * Entering the values 45.50 and 75 produces the result "The total cost of your purchase with your 10% discount of $12.08 is $108.45."
             * Entering the values 30 and 25 produces the result "The total cost of your purchase with your 5% discount of $2.75 is $52.25."
             * Entering the values 5.75 and 12.50 produces the result "The total cost of your purchase is $18.25."
             * Entering the values 666 and 666 produces the result "The total cost of your purchase, with your 10% discount of $133.2 is $1198.8."
             */

















        }

    }
}
