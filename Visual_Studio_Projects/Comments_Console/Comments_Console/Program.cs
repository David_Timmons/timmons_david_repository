﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comments_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            /* 
             * David Timmons
             * SDI Section
             * Assignment Name
             * Today's Date
             */
            //Doing the code for comments

            //Console.WriteLine will put a text string onto the console and a new line character at the end of it. 
            Console.WriteLine("This will print to the console.");
            Console.WriteLine("This is an example of a second line.");


            //Console.Write() - no new line character
            Console.Write("This is an example of a console write.\r\n");
            Console.Write("This should be on the same line.");
            Console.WriteLine("Welcome To SDI");
            Console.WriteLine("\r\n");

            //Carriage Return \r
            //New Line Character \n
            //We will use for a new line character \r\n

       
        }
    }
}
