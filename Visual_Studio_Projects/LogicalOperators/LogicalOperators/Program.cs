﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicalOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            //Logical Operators
            // And && Operator

            //Can we buy an iPhone?

            decimal budget = 600.00m;
            decimal iPhoneCost = 500.00m;

            Console.WriteLine("How big was your paycheck? Please enter a dollar value with two decimal places.");
            string paycheckString = Console.ReadLine();

            decimal paycheckDecimal = decimal.Parse(paycheckString);

            Console.WriteLine("Press enter to confirm that your paycheck was " +paycheckDecimal+".");
            string confirm = Console.ReadLine();



            //iPhone price has to be less than budget, AND paycheck has to be more than 700.00m

            if (iPhoneCost < budget && paycheckDecimal > 700.00m)
            {
                Console.WriteLine("You can purchase the iPhone.");
            }
            else
                Console.WriteLine("You cannot purchase the iPhone.");

            // Or || Operator

            // Not ! Operator

            if (!(iPhoneCost>budget))
            {
                Console.WriteLine("Because of the backwards ass way this was written, you can buy the iPhone.");
            }else
            {
                Console.WriteLine("Because of the backwards ass way this wass written, you cannot buy the iPhone.");
            }




        }
    }
}
