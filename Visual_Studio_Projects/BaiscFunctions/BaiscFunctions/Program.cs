﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaiscFunctions
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Before the function call");

            PrintEnjoy();

            //Create a function call that will start our PrintMore function
            //Put the name of the function and an ();
            PrintMore();


            Console.WriteLine("After the function call");

            PrintMore();

            PrintEnjoy();

           
        }
        public static void PrintEnjoy()
        {
            Console.WriteLine("I hope you enjoy the program!");
        }
        public static void PrintMore()
        {
            //Code that will run when I "call" this method/function
            string userName = "David";
            Console.WriteLine("Welcome "+userName+" to the program.");
            
        }


       
    }
}
