﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            //Assignment Operators
            /*
             * = Assignment Operator is read as "is"
             * ++ Assignment Operator Addition of 1 "a = a + 1"
             * -- Assignment Operator Subraction of 1 "a= a - 1"
             * += Assignment Operator Addition Assignment Operator a+=4; = "a = a + 4;"
             * -= Assignment Operator Subtraction Assignment Operaor a-=4; = "a = a - 4;"
             * /= Assignment Operator Division Assignment Operator a/=4; = "a = a / 4;"
             * *= Assignment Operator Multiplication Assignment Operator a*=4 = "a = a * 4"
             * Assignment Operators redefine the value of what they are assigned to. 
             
             
             
             */


            int toChange = 5; //Value is 5
            toChange++; //Value is 6
            toChange--; //Value is 5
            toChange += 3; // Value is 8
            toChange -= 2; // Value is 6
            toChange /= 2; // Value is 3
            toChange *= 5; // Value is 15
            Console.WriteLine(toChange);

            

        }
    }
}
