﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimmonsDavid_CE01
{
    class Program
    {
        static void Main(string[] args)
        {
            /* 
             * David Timmons
             * ASD Code Exercise 01
             * October 24, 2017
             * Section 01
             * 
             */

            //Declaring arrays and defining values
            string[] colorArray = new string[12] { "blue", "green", "red", "yellow", "purple", "blue", "blue", "green", "green", "red", "red", "yellow" };
            int[] lengthArray = new int[12] { 1, 12, 24, 64, 43, 23, 13, 76, 6, 54, 12, 13 };

            //Creating Menu, prompting user input
            Console.WriteLine("Welcome to the program where we measure imaginary fish.");
            Console.WriteLine("Please select an imaginary fish color: ");
            Console.WriteLine("1. Blue");
            Console.WriteLine("2. Green");
            Console.WriteLine("3. Red");
            Console.WriteLine("4. Yellow");
            Console.WriteLine("5. Purple");

            string userInputColor = Console.ReadLine();
            int colorInput;

            while (!int.TryParse(userInputColor, out colorInput))
            {
                Console.WriteLine("Enter a number");
                userInputColor = Console.ReadLine();

                while (colorInput < 0 && colorInput > 5)
                {
                    Console.WriteLine("Enter a number between 1 and 5");
                    userInputColor = Console.ReadLine();
                }
            }
        }
    }
}
