﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConditionalsBasic
{
    class Program
    {
        static void Main(string[] args)
        {

            //Boolean Variables 
            //Only contain a value of true or false

            //true is not a text string of true
            //Think of as on
            bool testing1 = true;
            //Think of as off
            bool wontRun = false;

            Console.WriteLine(testing1);
            Console.WriteLine(wontRun);

            //Relational Operators
            Console.WriteLine(5<7);
            Console.WriteLine(7>5);
            Console.WriteLine(2>5);

            Console.WriteLine(5<=5);
            Console.WriteLine(6<=6);

            //Equality/Inequality Operators
            // == Equality =  "is it the same as?"
            // != Inequality = "is it not the same as?"

            // ! = Negate in C#

            Console.WriteLine(4 == 4);
            Console.WriteLine(4 != 3);
            Console.WriteLine(4 != 4);

            //if statement

            int kidAge = 18;

            if (kidAge >= 13)
            {
                Console.WriteLine("The child can go to the PG13 movie");
            }
            else if(kidAge >= 10)
            {
                Console.WriteLine("The child can go to the PG movie");
            }
            else
            {
                Console.WriteLine("The child can only see G-rated movies.");
            }

            //elseif statements
            //evaluates a series of statements if the previous statement is false



            //Work in a shoestore
            //Get a bonus if we sell fifty or more pairs of shoes in a day

            //Declare and define starting variables

            decimal basePay = 200.00m;
            decimal bonus = 50.00m;

            //what is our total pay

            decimal totalPay = 0.00m;

            int shoesPerDay = 25;

            //create a conditional to test if we get our bonus

            if (shoesPerDay >= 50)
            {
                //we get our bonus
                totalPay = basePay + bonus;
            }
            else {
                //we do not get a bonus
                totalPay = basePay;
            }
            Console.WriteLine("Your total pay is $" +totalPay );
            
             





        }
    }
}
