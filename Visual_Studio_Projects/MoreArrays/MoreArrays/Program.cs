﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoreArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            //Function call to run arrayLists method
            nameArrayLists();
        }

        //Create a method for the array lists

        public static void nameArrayLists()
        {
            //Create an arraylist
            //Add an initial element
            ArrayList studentList = new ArrayList() {"Dan"};

            studentList.Add("Craig");
            studentList.Add("Josh");
            studentList.Add("Chad");
            studentList.Add("Rebecca");

            //create a foreach loop to cycle through the array

            //insert (int, element)

            studentList.Insert(0, "Lee");

            //remove a student

            studentList.Remove("Chad");

            //See if the arraylist contains an element

            Console.WriteLine("Contains {0}",studentList.Contains("Dan") );
            Console.WriteLine("Contains {0}?", studentList.Contains("Bob"));

            foreach (string student in studentList)
            {
                Console.WriteLine(student);
            }

            //arraylist properties

            //use the index number
            //get a single element 
            Console.WriteLine(studentList[2]);

            //set a single element
            studentList[3] = "Chris";
            Console.WriteLine(studentList[3]);

            foreach (string student in studentList)
            {
                Console.WriteLine(student);
            }

            //get the number of elements in the array list
            //number of elements in the arraylist currently

            Console.WriteLine("Count {0}" , studentList.Count);

            //The capacity is the number of elements that the arrayList can store
            //Will get bigger if count reaches capacity

            //capacity
            Console.WriteLine("Capacity {0}", studentList.Capacity);


        }

    }
}
