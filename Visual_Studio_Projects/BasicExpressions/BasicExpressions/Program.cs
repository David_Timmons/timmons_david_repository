﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicExpressions
{
    class Program
    {
        static void Main(string[] args)
        {
            //Basic Expressions 
            //Sequential Programming
            //Line By Line

            //Declare and define a variable
            int a = 2;
            //Change the value of a permanently
            a = a + 3;
                Console.WriteLine(a);

            //Create a new variable and base it on a different variable
            int b = a + 3;
            Console.WriteLine(b);

            //Expression that finds our age

            //Current year

            int currentYear = 2017;

                //Year you were born

            int yearBorn = 1992;

            //simple subtraction
            //create a variable to hold age

            int age = currentYear - yearBorn;

            Console.WriteLine("My current age is "+age);





           
        }
    }
}
