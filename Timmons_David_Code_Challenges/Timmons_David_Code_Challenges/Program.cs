﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timmons_David_Code_Challenges
{
    class Program
    {
        static void Main(string[] args)
        {
            //Providing output to user, accepting user input
            Console.WriteLine("Welcome to DVP1 Code Challenges!\r\nPlease select which function you would like to execute.");
            Console.WriteLine("1. Swap Name\r\n2. Backwards \r\n3. Age Convert\r\n4. Temp Convert\r\n5. Exit");
            string userInput = Console.ReadLine();
            //defining user inputs and validating what should be done
            while ((userInput == "1") || (userInput == "2") || (userInput == "3") || (userInput == "4") || (userInput != "5"))
            {
                //executing code paths and reprompting once code path has been executed
                if (userInput == "1")
                {
                    string nameSwapDone = SwapName();
                    Console.WriteLine("Your names swapped are {0}", nameSwapDone);
                    Console.WriteLine("\r\nPlease select which function you would like to execute.");
                    Console.WriteLine("1. Swap Name\r\n2. Backwards \r\n3. Age Convert\r\n4. Temp Convert\r\n5. Exit");
                    userInput = Console.ReadLine();
                }
                else if (userInput == "2")
                {
                    string[] backwardsDone = Backwards();
                    Console.WriteLine("\r\nPlease select which function you would like to execute.");
                    Console.WriteLine("1. Swap Name\r\n2. Backwards \r\n3. Age Convert\r\n4. Temp Convert\r\n5. Exit");
                    userInput = Console.ReadLine();
                }
                else if (userInput == "3")
                {
                    string AgeConvertDone = AgeConvert();
                    Console.WriteLine("\r\nPlease select which function you would like to execute.");
                    Console.WriteLine("1. Swap Name\r\n2. Backwards \r\n3. Age Convert\r\n4. Temp Convert\r\n5. Exit");
                    userInput = Console.ReadLine();
                }
                else if (userInput == "4")
                {
                    decimal tempConvertDone = TempConvert();
                    Console.WriteLine("\r\nPlease select which function you would like to execute.");
                    Console.WriteLine("1. Swap Name\r\n2. Backwards \r\n3. Age Convert\r\n4. Temp Convert\r\n5. Exit");
                    userInput = Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Please enter a number value between 1 and 4.");
                    userInput = Console.ReadLine();
                }
            }
        }
        //Creating custom method for first problem
        static string SwapName()
        {
            //prompting for user input
            Console.WriteLine("Please enter your first name and press enter.");
            string firstName = Console.ReadLine();
            //prompting for user input
            Console.WriteLine("Please enter your last name and press enter.");
            string lastName = Console.ReadLine();
            //providing output to user
            Console.WriteLine("The names you have entered are {0} and {1}",firstName,lastName);
            string swapped = lastName + " " + firstName;
            //returning the names swapped
            return swapped;
        }
        //creating the custom method for second problem
        static string[] Backwards()
        {
            //prompting for user input
            Console.WriteLine("Please enter a sentence with at least six words.");
            string sentence = Console.ReadLine();
            //splitting sentence into individual words
            //storing those individual words as elements in an array
            string[] sentenceArray = sentence.Split(' ');
            //validating that the last index of the array is greater than 5
            //so that the array has at least six elements in it
            while (sentenceArray.Length <= 5)
            {
                //reprompting if array was not long enough
                Console.WriteLine("Please enter a sentence with at least six words.");
                sentence = Console.ReadLine();
                sentenceArray = sentence.Split(' ');
            }
            //providing output to the user
            Console.WriteLine("The sentence you entered is {0}",sentence);
            //declaring variable to loop through the array
            int i = sentenceArray.Length;
            Console.Write("That sentence reversed is ");
            //looping through the array backwards, one iteration at a time
            //stopping loop when array index reaches 0
            while (i > 0)
            {
                i--;
                Console.Write(sentenceArray[i] + " ");
            }
            //adding an extra space for organization sake in the console
            Console.WriteLine("\r\n");
            //returning reversed sentence as an array iterated backwards
            return sentenceArray;
        }
        //creating custom class for age convert problem.
        static string AgeConvert()
        {
            //prompting for user input
            Console.WriteLine("Please enter your name.");
            string name = Console.ReadLine();
            //prompting for user input
            Console.WriteLine("Please enter your age.");
            string age = Console.ReadLine();
            //validating that user input is in the correct format and reprompting if it is not
            decimal years;
            while (!decimal.TryParse(age, out years)) 
            {
                Console.WriteLine("Please enter an integer value for your age.");
                age = Console.ReadLine();
            }
            //performing arithmetic on user input to gather required information
            decimal days = ((years * 365) + (years / 4));
            decimal hours = days * 24;
            decimal minutes = hours * 60;
            decimal seconds = minutes * 60;
            //outputing information based on user input
            Console.WriteLine("{0}, you have been alive for {1} days, including leap days,\r\n{2} hours,\r\n{3} minutes,\r\nand {4} seconds.", name, days, hours, minutes, seconds);
            return name;
        }
        //creating custom method for temp convert problem
        static decimal TempConvert()
        {
            //prompting for user input
            Console.WriteLine("Please enter a temperature in Fahrenheit.");
            string f = Console.ReadLine();
            //validating the user input is in the correcrt format and reprompting if it is not
            decimal fDecimal;
            while (!decimal.TryParse(f, out fDecimal)) 
            {
                Console.WriteLine("Please enter a number value for your temperature in Fahrenheit.");
                f = Console.ReadLine();
            }
            //providing output to user
            Console.WriteLine("The temperature you entered is {0} degrees Fahrenheit.",fDecimal);
            //performing conversion on user input and providing output to user
            decimal cDecimal = ((fDecimal - 32) * 5 / 9);
            Console.WriteLine("That temperature converted to Celsius is {0} degrees Celsius.",cDecimal);
            //prompting for user input
            Console.WriteLine("Please enter a temperature in Celsius.");
            string c = Console.ReadLine();
            //validating the user input is in the correct format and reprompting if it is not
            decimal cDecimal1;
            while (!decimal.TryParse(c, out cDecimal1))
            {
                Console.WriteLine("Please enter a number value for your temperature in Celsius.");
                c = Console.ReadLine();
            }
            //providing output to user
            Console.WriteLine("The temperature you entered is {0} degrees Celsius.",cDecimal1);
            //performing conversion on user input and providing output to user
            decimal fDecimal1 = cDecimal1 * 9 / 5 + 32;
            Console.WriteLine("That temperature converted to Fahrenheit is {0} degrees Fahrenheit.",fDecimal1);

            //returning value to function 
            return fDecimal;

        } 
    }
}
